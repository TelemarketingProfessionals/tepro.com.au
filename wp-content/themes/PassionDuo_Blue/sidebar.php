<!-- begin sidebar -->

<div id="sidebar">

<? include(TEMPLATEPATH."/ads.php"); ?>

<div class="search"><form id="searchform" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>"><input type="text" name="s" id="s" value="Type and hit Enter to Search" onfocus="if (this.value == 'Type and hit Enter to Search') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Type and hit Enter to Search';}" /></form></div>

<div id="sidebar_l">
 <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Left Sidebar') ) : else : ?>


<ul>
<li><h2>Recent Posts</h2></li>
<li><ul>
<?php get_archives('postbypost', 5); ?>
</ul></li>
</ul>

<ul>
<li><h2>Comments</h2></li>
<li>
<?php include (TEMPLATEPATH . '/simple_recent_comments.php'); /* recent comments plugin by: www.g-loaded.eu */?>
<?php if (function_exists('src_simple_recent_comments')) { src_simple_recent_comments(5, 60, '', ''); } ?>
</li>
</ul>



<?php endif; ?>

</div>





<div id="sidebar_r">

 <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Right Sidebar') ) : else : ?>

<ul>
<li><h2>Categories</h2></li>
<li><ul>
<?php wp_list_cats("sort_column=name&optioncount=0&hierarchical=0"); ?>
</ul></li>
</ul>


<?php endif; ?>

</div>


</div>

<!-- end sidebar -->