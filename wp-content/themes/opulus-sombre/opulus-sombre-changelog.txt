v1.3.9  - Added featured images to frontpage at mobile and center titles and text
        - Fixed margins on align classes
        - Updated theme options panel

v1.3.8  - Moved content_width global into after_setup_theme

v1.3.7	- Added meta title function to fix blank frontpage title issue

v1.3.6	- Added FontAwesome font
	- Fixed mobile logo layout

v1.3.5	- Fixed textarea filter

v1.3.4	- Fixed incorrect $content_width variable

v1.3.3 	- Prefixed all variables
	- Removed enqueue for jQuery as it's called as a dependancy

v1.3.2 	- Trimmed all trailing whitespace
	- Converted EOL to windows to match wp core
	- Added pluggable condition to many functions for child theme use

v1.3.1 	- Added option to hide rss feed icon
	- Added option to use alternate rss url
	- Added esc_url validatio to all social media links on output

v1.3.0 	- Fixed broken inlclude for featured spots on the front

v1.2.9 	- Removed unused script: jquery.xcolor.min.js

v1.2.8 	- Fixed image alignment issues on frontpage
	- Fixed transparency issue with ribbon_wrap

v1.2.7	- Created title partials and updated any templates with calls to post/page titles
	- Moved blog feed loop to partial and updated templates
	- Added option to show excerpt/content on blog feeds
	- Updated supportinfo url
	- Removed option to set blog post number so number will be set by core feature
	- Cleaned page.php template
	- Removed author template as it is replced by archive with conditions in partial
	- Moved content from header to partials
	- Created a clear div helper function
	- Fixed error on comments.php causing warning

v1.2.6	- Remove custom-header tag
	- Fixed menu/logo overlap issue with large menus

v1.2.5	- Fixed errors caused by missing array key in options panel

v1.2.4	- Fixed errors reported from new sanitize functions

v1.2.3	- Updated sanatize functions in theme options panel

v1.2.2	- Removed unused constants CSS_PATH and JS_PATH
	- Added constants for sales and support pages
	- Added Unique keys to option array
	- Added loop to auto poulate options form panes
	- Added support info buttons and supporting css
	- Updated Setup tab on options panel
	- Removed function saving default options to DB on init
	- Cleaned up sanitize function for theme options
	- Updated get_options function to pull defaults from options array

v1.2.1	- Use get_option('date_format') per discussion: "[theme-reviewers] Hard coding date format"
	- Migrated to new layout tags

v1.2.0	- Moved social icons into partial
	- Updated and shortened credit link in footer
	- Removed refrences/hooks/functions/sidbars for post-type not availible in free theme

v1.1.9	- Fixed default read more text causing options to fail during update in some hosts

v1.1.8	- Fixed Sidebar menu visibility	CSS issue
	- Added missing default widget classes to register_sidebar
	- Fixed broken logo link on mobile menu
	- Updated marketing link urls with global variable

v1.1.7	- Added PO/MO files for translation

v1.1.6	- Move elements into partials: no content error, logo, title

v1.1.5	- Updated paths to js/css/images to work with child themes

v1.1.4	- Added Google Plus icon in footer
	- Fixed letter spacing issue causing odd text display

v1.1.3	- Fixed spacing issue in theme options title
	- Added condition to hide Action Text when left blank in options panel

v1.1.2	- Fixed checkboxes not saving in options panel due to obtuse isset condition

v1.1.1	- Added option to set footer background color

v1.1.0	- Fixed Localization namespace issue on options.php
	- Simplified load demo funtion and removed secondary options table row to conform to directory guidelines
	- Fixed Undefined index notices caused by filter process
	- Simplified sanitize_callback to remove filters with little/no value

v1.0.9	- Updated and simplified function nimbus_options_to_head to automate font filtering and setting of option variables

v1.0.8	- Fixed conflict with WP e-Commerce in nimbus_public.js

v1.0.7	- Removed Option to include additional site-wide js libraries to fix conflict with mobile menu and conform to theme directory guidelines on external resources

v1.0.6	- Removed jquery.nivo.slider.js orphaned script
	- Removed jquery.nivo.slider.js from license exceptions on readme
	- Internationalized all non translatable text on tamplates
	- Internationalized all non translatable text in options panel

v1.0.5	- Added "Respond.js v1.1.0: min/max-width media query polyfill" to fix header display issue in ie7-ie8
	- Fixed conflict with "All-in-One Event Calendar by Timely" by prefixing post_meta_single with nimbus_ in option array and on single.php and header.php

v1.0.4	- Fixed layout issue caused by introduction of wp_enqueue_styles by switching to wp_enqueue_scripts

v1.0.3 	- Fixed content area overflow issue with oversized images with caption
	- Fixed categroy text spacing on single.php
	- Added pagination to single.php
	- Added post format pagination
	- Added post format previous/next image thumbs
	- Fixed search returning wrong results
	- Fixed search results layout issues
	- Removed 'portfolio sidebar' condition and output
	- Updated wp_print_styles with wp_enqueue_styles

v1.0.2 	- Updated copyright and license
	- Updated license to GPL V3
	- Added readme.txt with license info on all scripts/fonts
	- Removed orphaned font Museo
	- Added non-min version of all minified scripts and stylesheets
	- Removed support widget from dashboard
	- Updated all !function_exists to is_active on sidebars
	- Update credit link to point to theme page
	- Removed wp_deregister_script for jquery
	- Added wp_enqueue_script for jQuery from core
	- Updated all dirname(__FILE__) to get_template_directory()
	- Removed orphaned options causing errors after saving options panel

v1.0.1	- Removed function_exists() conditional from call to register_nav_menu

v1.0.0 	- Initial Launch