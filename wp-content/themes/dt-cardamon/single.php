<?php get_header(); ?>
<?php dt_storage('have_sidebar', true); ?>
    
    <?php get_template_part('top-bg'); ?>
    
    <?php get_template_part('parallax'); ?>
    
    <div id="wrapper">
        
        <?php get_template_part('nav'); ?>
        
        <div id="container">
            
            <?php if( have_posts() ): while( have_posts() ): the_post(); ?>
            
            <h1><?php the_title(); ?></h1>
            
            <?php if( !post_password_required() ): ?> 
            
			<?php if( of_get_option('misc-show_next_prev_post') ): ?>
			
			<div class="paginator-r inner-navig">
                <?php
                global $wpdb, $post;
                $result = $wpdb->get_results( "SELECT ID FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY post_date DESC" );
                
                $current = 1;
                foreach( $result as $index=>$obj ) {
                    if( $obj->ID == $post->ID )
                        $current = $index + 1;
                }
                ?>
                
                    <span class="pagin-info"><?php printf( __('Post %d of %d', LANGUAGE_ZONE), $current, count($result) ); ?></span>

                <?php

                // next link
                ob_start();
                next_post_link('%link', '');
                $link = ob_get_clean();
                if( $link ) {
                    echo str_replace('href=', 'class="prev" href=', $link);
                }else
                    echo '<a href="#" class="prev no-act" onclick="return false;"></a>';

                // previos link
                ob_start();
                previous_post_link('%link', '');
                $link = ob_get_clean();
                if( $link ) {
                    echo str_replace('href=', 'class="next" href=', $link);
                }else
                    echo '<a href="#" class="next no-act" onclick="return false;"></a>';

                ?>
            </div>
            
			<?php endif; ?>
			
            <div class="hr hr-wide gap-small"></div>

            <?php
                    get_template_part('single', 'dt_blog');

                    else:?>
                        <div class="hr hr-wide gap-small"></div>
                        <?php echo get_the_password_form(); ?>
                <?php endif;// password protection

                endwhile;
            endif;
            ?>
            

        </div>

        <aside id="aside" class="right">
        <?php dt_widget_area( '', null, 'sidebar_3' ); ?>
        </aside>
    
    </div>

<?php get_footer(); ?>
