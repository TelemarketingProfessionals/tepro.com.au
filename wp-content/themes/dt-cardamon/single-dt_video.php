<?php get_header(); ?>
    
    <?php get_template_part('top-bg'); ?>
    
    <?php get_template_part('parallax'); ?>
    
    <div id="wrapper">
        
        <?php get_template_part('nav'); ?>
        
        <div id="container" class="full-width for-gal in">
            
            <?php if( have_posts() ): while( have_posts() ): the_post(); ?>
            
            <h1><?php the_title(); ?></h1>
            <div class="hr hr-wide gap-big"></div>

            <?php
            global $post;
            $opts = get_post_meta($post->ID, '_dt_video_options', true);
            if( !empty($opts['video_link']) ):
            ?>

            <div class="videos"> 

                <?php dt_get_embed( $opts['video_link'], $opts['width'], $opts['height'] ); ?>

            </div>

            <?php endif; ?>

            <div class="full-left">
                    
                <?php
                the_excerpt();
                dt_get_taxonomy_link(
                    'dt_video_category',
                    '<p>' . __('Category: ', LANGUAGE_ZONE) . '%CAT_LIST%</p>'
                );
                ?>

            </div>
            
            <?php endwhile; endif; ?>
        
        </div><!-- #container -->
    
    </div><!-- #wrapper -->

<?php get_footer(); ?>
