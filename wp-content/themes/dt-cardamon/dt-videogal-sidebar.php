<?php
/* Template Name: 13. Video Gallery with Sidebar */
?>

<?php get_header(); ?>
<?php dt_storage('have_sidebar', true); ?>

<?php get_template_part('top-bg'); ?>

<?php get_template_part('parallax'); ?>

<div id="wrapper">

    <?php get_template_part('nav'); ?>
    
    <?php get_sidebar( 'left' ); ?>

    <div id="container">

        <h1><?php the_title(); ?></h1>
        <div class="hr hr-wide gap-big"></div>

        <div class="gallery">
           
            <?php
            global $post;
            $opts = get_post_meta($post->ID, '_dt_video_layout_options', true);
            $cats = get_post_meta($post->ID, '_dt_video_layout_category', true);

            dt_category_list( array(
                'post_type'         => 'dt_video',
                'taxonomy'          => 'dt_video_category',
                'select'            => $cats['select'],
                'layout'            => $opts['layout'],
                'layout_switcher'   => false,
                'show'              => ('on' == $opts['show_cat_filter'])?true:false,
                'terms'             => isset($cats['video_cats'])?$cats['video_cats']:array() 
            ) ); 
            ?>
            
            <div class="gallery-inner t-l dt-ajax-content video-gal"></div>
        </div>
    </div>
        
        <?php get_sidebar( 'right' ); ?>

</div>
<?php get_footer(); ?>
