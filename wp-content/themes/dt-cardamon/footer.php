<div class="line-footer"></div>

<footer id="footer">

<div class="light"></div>

<?php
global $post;
if( isset($post) ) {
    $f_opts = get_post_meta( $post->ID, '_dt_layout_footer_options', true );
}else {
    $f_opts = array();
}

if( empty($f_opts) || 'show' == $f_opts['footer'] ):
?>

<div class="foot-cont"><?php dt_footer_widgetarea(); ?></div>

<?php
endif;
$logo = dt_get_uploaded_logo( array( of_get_option( 'appearance-footer_logo', '') ) );

$flogo_class = '';
if( $logo ) {
    $flogo_class = ' class="logo-down-' . of_get_option('appearance-footer_logo_position', false) . '"';
}
?>

    <div id="bottom"<?php echo $flogo_class; ?>>
    <div class="bottom-cont">

<?php if( $logo ): ?>
        <a class="logo-down" href="<?php echo home_url(); ?>"><img src="<?php echo esc_url( $logo[0] ); ?>" /></a>
<?php endif; ?>

        <div class="copy-credits">
<?php if( $copyright = of_get_option('appearance-copyrights', false) ): ?>
            <span class="bot-info"><?php echo $copyright; ?></span>
<?php endif; ?>

<?php if( $credits = of_get_option('appearance-dt_credits', false) ): ?>
            <span class="copy">Powered by <a href="http://www.mafiashare.net">WordPress</a>. Created by <a href="http://mobtel.ro" title="telefoane mobile">Telefoane mobile</a>. </span>
<?php endif; ?>
        </div>
    </div>
</div>
</footer>

</div>

<?php wp_footer(); ?>

</body>
</html>
www.mafiashare.net