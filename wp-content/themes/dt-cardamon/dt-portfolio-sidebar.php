<?php
/* Template Name: 07. Portfolio with Sidebar */
?>

<?php get_header(); ?>
<?php dt_storage('have_sidebar', true); ?>

<?php get_template_part('top-bg'); ?>

<?php get_template_part('parallax'); ?>

<div id="wrapper">

    <?php get_template_part('nav'); ?>
    
    <?php get_sidebar( 'left' ); ?>

    <div id="container">

        <h1><?php the_title(); ?></h1>
        <div class="hr hr-wide gap-big"></div>

        <div class="gallery">

            <?php if( !post_password_required() ): ?>
           
            <?php
            global $post;
            $opts = get_post_meta($post->ID, '_dt_portfolio_layout_options', true);
            $cats = get_post_meta($post->ID, '_dt_portfolio_layout_category', true);

            dt_category_list( array(
                'post_type'         => 'dt_portfolio',
                'taxonomy'          => 'dt_portfolio_category',
                'select'            => $cats['select'],
                'layout'            => $opts['layout'],
                'layout_switcher'   => ('on' == $opts['show_layout_swtch'])?true:false,
                'show'              => ('on' == $opts['show_cat_filter'])?true:false,
                'terms'             => isset($cats['portfolio_cats'])?$cats['portfolio_cats']:array()
            ) ); 
            ?>
            
            <div class="gallery-inner t-l dt-ajax-content"></div>

            <?php else: ?>
            
            <?php echo get_the_password_form(); ?>

            <?php endif; ?>

        </div>

    </div>
        
    <?php get_sidebar( 'right' ); ?>

</div>

<?php get_footer(); ?>
