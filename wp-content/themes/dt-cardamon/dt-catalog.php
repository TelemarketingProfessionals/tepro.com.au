<?php
/* Template Name: 14. Catalog */
?>
<?php get_header(); ?>
<?php dt_storage('have_sidebar', true); ?>

<?php get_template_part('top-bg'); ?>

<?php get_template_part('parallax'); ?>

<div id="wrapper">

    <?php get_template_part('nav'); ?>
    
    <?php get_sidebar( 'left' ); ?>

    <div id="container">
        <h1><?php the_title(); ?></h1>
        <div class="hr hr-wide gap-big"></div>        

        <div class="gallery">
        <?php
        global $post;
        $opts = get_post_meta($post->ID, '_dt_catalog_layout_options', true);
        $cats = get_post_meta($post->ID, '_dt_catalog_layout_category', true);

        dt_category_list( array(
            'post_type'         => 'dt_catalog',
            'taxonomy'          => 'dt_catalog_category',
            'select'            => $cats['select'],
            'layout'            => '2_col-list',
            'show'              => ('on' == $opts['show_cat_filter'])?true:false,
            'layout_switcher'   => false,
            'terms'             => isset($cats['catalog_cats'])?$cats['catalog_cats']:array()
        ) ); 
        ?>
        
        <div class="dt-ajax-content"></div>
        </div>
    </div>
        
    <?php get_sidebar( 'right' ); ?>

</div>

<?php get_footer(); ?>
