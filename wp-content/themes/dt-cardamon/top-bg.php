<div id="top-bg">
    <div class="top-cont">
<?php
$menu_name = 'top-menu';

if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
	$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
    
    if( $menu ) {
	    $menu_items = wp_get_nav_menu_items($menu->term_id);

	    $menu_list = '<ul class="right-top">';

	    foreach ( (array) $menu_items as $key => $menu_item ) {
	        $title = $menu_item->title;
	        $url = $menu_item->url;
	        $menu_list .= '<li><a href="' . $url . '">' . $title . '</a></li>';
	    }
	    $menu_list .= '</ul>';
        echo $menu_list;
    }
}
?>
        <?php if( of_get_option('misc-show_search_top') ) get_search_form(); ?>
    </div>
</div>
