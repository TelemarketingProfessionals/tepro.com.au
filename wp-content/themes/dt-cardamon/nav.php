<?php $header_position = of_get_option('appearance-header_logo_position', 'center'); ?>
<header id="header" class="logo-<?php echo $header_position; ?>">

<?php
$logo = dt_get_uploaded_logo( array( of_get_option( 'appearance-header_logo', '' ) ) );
if ( $logo ): ?>
    <a href="<?php echo home_url(); ?>" class="logo"><img src="<?php echo esc_url( $logo[0] ); ?>"/></a>
<?php endif; ?>

<?php
$contact_fields = array(
    array(
        'prefix'    => 'address',
        'ico_class' => 'adress' 
    ),
    array(
        'prefix'    => 'phone',
        'ico_class' => 'ico-phone' 
    ),
    array(
        'prefix'    => 'email',
        'ico_class' => 'ico-mail' 
    ),
    array(
        'prefix'    => 'skype',
        'ico_class' => 'ico-scype' 
    )
);
if( of_get_option('misc-show_header_contacts', false) ): ?>

<div class="contact-block">

<?php
foreach( $contact_fields as $field ) {
    if( $data = of_get_option('misc-contact_' . $field['prefix'], false) ): 
        echo '<span class="' . esc_attr($field['ico_class']) . '">' . $data . '</span>';
    endif;
}
?>

</div>

<?php endif; ?>

<nav>

<?php
dt_menu( array(
    'menu_wraper' 	=> '<ul id="nav">%MENU_ITEMS%</ul>',
    'menu_items'	=> '<li %IS_FIRST%><a class="%ITEM_CLASS%" href="%ITEM_HREF%" title="%ESC_ITEM_TITLE%">%ITEM_TITLE%</a>%SUBMENU%</li>',
    'submenu' 		=> '<div style="visibility: hidden; display: block;"><ul>%ITEM%</ul><i></i></div>'
) );

dt_get_soc_links();
?>
</nav>
</header>
