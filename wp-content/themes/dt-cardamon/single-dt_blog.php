<div class="entry-meta">
<?php
dt_get_date_link( array('class' => 'ico-link date', 'wrap' => '<span class="%CLASS%">%DATE%</span>') );
dt_get_author_link( array('class' => 'ico-link author') );
dt_get_taxonomy_link( 'category', '<span class="ico-link categories">%CAT_LIST%</span>' );
dt_get_comments_link( '<a href="%HREF%" class="ico-link comments">%COUNT%</a>', array( 'text' => array( '0', '1', '%' ), 'no_coments' => '' ) );
?>
</div>

<?php
global $post;
$big = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
$post_opts = get_post_meta( $post->ID, '_dt_meta_post_options', true );
if( $big && (!isset($post_opts['hide_thumb']) || !$post_opts['hide_thumb']) ) {
    $big[3] = image_hwstring( $big[1], $big[2] );
    $thumb = dt_get_resized_img($big, array('w' => 202, 'h' => 202));
    printf('<a class="alignleft highslide" href="%1$s" onclick="return hs.expand(this)"><img src="%2$s" %3$s alt="%4$s" title="%4$s"></a>',
        $big[0], $thumb[0], $thumb[3], get_the_title()
    );
}
the_content();
wp_link_pages();
dt_get_taxonomy_link( 'post_tag', '<p><span class="ico-link categories">' . __('Tags: ', LANGUAGE_ZONE) . '%CAT_LIST%</span></p>' );
?>

<?php if( of_get_option('misc-show_author_details') ): ?>

<p class="gap"></p>
<p class="hr hr-narrow gap-small"></p>
<div class="about-autor full-width">
    <?php echo str_replace( "class='", "class='alignleft ", get_avatar( get_the_author_meta('ID'), 72 ) ); ?>
<p class="autor-head"><?php printf(
    __('This article was written by %s', LANGUAGE_ZONE),
    get_the_author_meta('nickname')
); ?></p>
<p><?php echo get_the_author_meta('description'); ?></p>
</div>

<?php endif; ?>

<p class="gap"></p>

<?php comments_template(); ?>
