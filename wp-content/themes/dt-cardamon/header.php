<!DOCTYPE html>
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=998">


<title>
	<?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		echo " | $site_description";
	}

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 ) {
		echo ' | ' . sprintf( __( 'Page %s', LANGUAGE_ZONE ), max( $paged, $page ) );
	}
	?>
</title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php
$icon = of_get_option( 'appearance-favicon' );
echo empty($icon)?'':'<link rel="icon" type="image/png" href="' . ( (strpos($icon, '/wp-content') === 0)?get_site_url().$icon:$icon ) . '" />'; 
?>

<?php wp_head(); ?>   
<?php dt_style_options(); ?>

<?php echo of_get_option( 'misc-analitics_code', '' ); ?>

    <script type="text/javascript">
    /* <![CDATA[ */
	// DO NOT REMOVE!
	// b21add52a799de0d40073fd36f7d1f89
	if( typeof window['hs'] !== 'undefined' ) {
        hs.graphicsDir = '<?php echo get_template_directory_uri(); ?>/js/plugins/highslide/graphics/';
    }
	/* ]]> */
    
    </script>
<?php if ( is_singular() ) wp_enqueue_script( "comment-reply" ); ?>
</head>

<body <?php body_class(); ?>>
<?php
global $post;
$class = array();
$template = dt_core_get_template_name();
if( false !== strpos( $template, 'dt-slideshow-' )  ) {
    $slider_options = get_post_meta( $post->ID, '_dt_slider_layout_options', true );
    if( $slider_options && isset($slider_options['slider']) ) {
        switch( $slider_options['slider'] ) {
            case 'carousel': $class[] = 'carous'; break;
            case 'photo_stack': $class[] = 'vert';
        }
    }
}

$class = implode( ' ', $class );
if( !empty($class) ) {
    $class = ' class="' . $class . '"';
}
?>
    <div id="<?php dt_main_block_class_changer(); ?>"<?php echo $class; ?>>
