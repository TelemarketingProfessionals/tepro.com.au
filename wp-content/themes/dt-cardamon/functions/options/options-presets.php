<?php
/*
 * options-presets.php
 */
$options[] = array( "name" => _x('Skins', 'theme-options', LANGUAGE_ZONE), "type" => "heading" );

$options[] = array(	"name" => _x('Select a skin', 'theme-options', LANGUAGE_ZONE), "type" => "block_begin" );

$options[] = array(
    "name"      => '',
    "desc"      => '',
    "id"        => "of-preset",
    "std"       => 'ancient_marble', 
    "type"      => "images",
    "options"   => array(
        'ancient_marble'    => '/images/backgrounds/ancient.jpg',
        'classic_restuarant'=> '/images/backgrounds/rest.jpg',
        'fabulous'          => '/images/backgrounds/fab.jpg',
        'geometric'         => '/images/backgrounds/geo.jpg',
        'kids_and_parents'  => '/images/backgrounds/kids.jpg',
        'medical_clinick'   => '/images/backgrounds/med.jpg',
        'minimalist'        => '/images/backgrounds/min.jpg',
        'origami'           => '/images/backgrounds/origami.jpg',
        'spa_salon'         => '/images/backgrounds/spa.jpg',
        'tatoo_salon'       => '/images/backgrounds/tattoo.jpg',
		'boxed_light'		=> '/images/backgrounds/light.jpg',
		'boxed_dark'		=> '/images/backgrounds/dark.jpg'
    ) 
);

$options[] = array(	"type" => "block_end");
