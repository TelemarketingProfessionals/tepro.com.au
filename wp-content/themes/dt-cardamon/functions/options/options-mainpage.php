<?php
/*
 * options-mainpage.php
 */
$options[] = array( "page_title" => "Appearance", "menu_title" => "Appearance", "menu_slug" => "of-appearance-menu", "type" => "page" );

//BRANDING
$options[] = array( "name" => _x('Branding', 'theme-options', LANGUAGE_ZONE), "type" => "heading" );

$options[] = array(	"name" => _x('Logo in header', 'theme-options', LANGUAGE_ZONE), "type" => "block_begin" );

    // logo
    $options[] = array(
        "name"  => '',
        "desc"  => '',
        "id"    => 'appearance-header_logo',
        "type"  => 'upload',
        'std'   => str_replace( site_url(), '', get_template_directory_uri() . '/images/origami/logo.png' )
    );

    // logo horizontal position
    $options[] = array(
        "name"      => '',
        "desc"      => _x('Logo position', 'theme-options', LANGUAGE_ZONE),
        "id"        => "appearance-header_logo_position",
        "std"       => "center",
        "type"      => "select",
        "class"     => "mini",
        "options"   => $h_arr
    );

$options[] = array(	"type" => "block_end");

$options[] = array(	"name" => _x('Logo in footer', 'theme-options', LANGUAGE_ZONE), "type" => "block_begin");

    // logo
    $options[] = array(
        "name"  => '',
        "desc"  => '',
        "id"    => 'appearance-footer_logo',
        "type"  => 'upload',
        'std'   => str_replace( site_url(), '', get_template_directory_uri() . '/images/origami/logo-down.png' )
    );

    // logo horizontal position
    $options[] = array(
        "name"      => '',
        "desc"      => _x('Logo position', 'theme-options', LANGUAGE_ZONE),
        "id"        => "appearance-footer_logo_position",
        "std"       => "center",
        "type"      => "select",
        "class"     => "mini",
        "options"   => $h_arr
    );

$options[] = array(	"type" => "block_end");

$options[] = array(	"name" => _x('Favicon', 'theme-options', LANGUAGE_ZONE), "type" => "block_begin");

    // favicon
    $options[] = array( "name" => '', "desc" => '', "id" => "appearance-favicon", "type" => "upload" );

$options[] = array(	"type" => "block_end");

$options[] = array(	"name" => _x('Copyright & Credits', 'theme-options', LANGUAGE_ZONE), "type" => "block_begin");

    $options[] = array(
        "name"  => '',
        "desc"  => _x('Copyright information', 'theme-options', LANGUAGE_ZONE),
        "id"    => "appearance-copyrights",
        "std"   => false,
        "type"  => "textarea"
    );

    $options[] = array(
        "name"      => '',
        "desc"      => _x('Give credits to Dream-Theme', 'theme-options', LANGUAGE_ZONE),
        "id"        => "appearance-dt_credits",
        "std"       => "1",
        "type"      => "checkbox",
        'options'   => array( 'java_hide' => true )
    );
    
    $options[] = array( 'type' => 'js_hide_begin' );

        $options[] = array( "desc"  => 'Thank you so much!', "type" => "info" );
    
    $options[] = array( 'type' => 'js_hide_end' ); 

$options[] = array(	"type" => "block_end");

// MISC
$options[] = array( "name" => _x('Misc', 'theme-options', LANGUAGE_ZONE), "type" => "heading" );

$options[] = array(	"name" => _x('Contacts in header', 'theme-options', LANGUAGE_ZONE), "type" => "block_begin" );

    $options[] = array(
        "name"      => '',
        "desc"      => _x('Show contacts in header', 'theme-options', LANGUAGE_ZONE),
        "id"        => "misc-show_header_contacts",
        "std"       => "0",
        "type"      => "checkbox",
        'options'   => array( 'java_hide' => true )
    );
    
    $contact_fields = array(
        array(
            'prefix'    => 'address',
            'desc'      => _x('Adress', 'theme-options', LANGUAGE_ZONE) 
        ),
        array(
            'prefix'    => 'phone',
            'desc'      => _x('Phone', 'theme-options', LANGUAGE_ZONE) 
        ),
        array(
            'prefix'    => 'email',
            'desc'      => _x('Email', 'theme-options', LANGUAGE_ZONE) 
        ),
        array(
            'prefix'    => 'skype',
            'desc'      => _x('Skype', 'theme-options', LANGUAGE_ZONE) 
        ),
    );

    $options[] = array( 'type' => 'js_hide_begin' );

        foreach( $contact_fields as $field ) {
        
            $options[] = array(
                "name"      => '',
                "desc"      => $field['desc'],
                "id"        => "misc-contact_" . $field['prefix'],
                "std"       => "",
                "type"      => "text",
                'sanitize'  => false
            );

        }

    $options[] = array(	"type" => "block_end");

$options[] = array(	"type" => "block_end");

$options[] = array(	"name" => _x('Show next/prev links', 'theme-options', LANGUAGE_ZONE), "type" => "block_begin");

    // show prev/next in post
    $options[] = array(
        "name"  => "",
        "desc"  => _x('on blog post page', 'theme-options', LANGUAGE_ZONE),
        "id"    => "misc-show_next_prev_post",
        "std"   => "1",
        "type"  => "checkbox"
    );

	// show prev/next in portfolio
    $options[] = array(
        "name"  => "",
        "desc"  => _x('on portfolio project page', 'theme-options', LANGUAGE_ZONE),
        "id"    => "misc-show_next_prev_portfolio",
        "std"   => "1",
        "type"  => "checkbox"
    );
	
$options[] = array(	"type" => "block_end");

$options[] = array(	"name" => _x('Search form in the top line', 'theme-options', LANGUAGE_ZONE), "type" => "block_begin");

    // show search
    $options[] = array(
        "name"  => "",
        "desc"  => _x('Show search form', 'theme-options', LANGUAGE_ZONE),
        "id"    => "misc-show_search_top",
        "std"   => "1",
        "type"  => "checkbox"
    );
	
$options[] = array(	"type" => "block_end");

$options[] = array(	"name" => _x('Post options', 'theme-options', LANGUAGE_ZONE), "type" => "block_begin");

    // show author details
    $options[] = array(
        "name"  => "",
        "desc"  => _x('Show author info in posts', 'theme-options', LANGUAGE_ZONE),
        "id"    => "misc-show_author_details",
        "std"   => "1",
        "type"  => "checkbox"
    );
	
$options[] = array(	"type" => "block_end");

$options[] = array(	"name" => _x('Make parent menu items clickable', 'theme-options', LANGUAGE_ZONE), "type" => "block_begin");

    // parent menu clickable
    $options[] = array(
        "name"  => "",
        "desc"  => _x('Enable', 'theme-options', LANGUAGE_ZONE),
        "id"    => "misc-parent_menu_clickable",
        "std"   => "0",
        "type"  => "checkbox"
    );

$options[] = array(	"type" => "block_end");

$options[] = array(	"name" => _x('Analytics', 'theme-options', LANGUAGE_ZONE), "type" => "block_begin");

    $options[] = array(
        "name"      => '',
        "desc"      => _x('Analytics code', 'theme-options', LANGUAGE_ZONE),
        "id"        => "misc-analitics_code",
        "std"       => false,
        "type"      => "textarea",
        "sanitize"  => false
    );

$options[] = array(	"type" => "block_end");
