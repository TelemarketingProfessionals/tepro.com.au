<?php get_header(); ?>
    
    <?php get_template_part('top-bg'); ?>
    
    <?php get_template_part('parallax'); ?>
    
    <div id="wrapper">
        
        <?php get_template_part('nav'); ?>
        
        <div id="container" class="full-width for-gal in">
            
            <?php if( have_posts() ): while( have_posts() ): the_post(); ?>
            
            <h1><?php the_title(); ?></h1>
            
            <?php if( !post_password_required() ): ?>
			
			<?php if( of_get_option('misc-show_next_prev_portfolio') ): ?>
            
			<div class="paginator-r inner-navig">
                <?php
                global $wpdb, $post;
                $result = $wpdb->get_results( "SELECT ID FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'dt_portfolio' ORDER BY post_date DESC" );
                
                $current = 1;
                foreach( $result as $index=>$obj ) {
                    if( $obj->ID == $post->ID )
                        $current = $index + 1;
                }
                ?>
                
                    <span class="pagin-info"><?php printf( __('Project %d of %d', LANGUAGE_ZONE), $current, count($result) ); ?></span>

                <?php

                // next link
                ob_start();
                next_post_link('%link', '');
                $link = ob_get_clean();
                if( $link ) {
                    echo str_replace('href=', 'class="prev" href=', $link);
                }else
                    echo '<a href="#" class="prev no-act" onclick="return false;"></a>';

                // previos link
                ob_start();
                previous_post_link('%link', '');
                $link = ob_get_clean();
                if( $link ) {
                    echo str_replace('href=', 'class="next" href=', $link);
                }else
                    echo '<a href="#" class="next no-act" onclick="return false;"></a>';

                ?>
            </div>
			
			<?php endif; ?>

            <div class="hr hr-wide gap-big"></div>

            <?php
            global $post;
            $dt_tmp_query = new WP_Query();
            $post_opts = get_post_meta($post->ID, '_dt_portfolio_options', true);

            if( !isset($post_opts['hide_media']) || (isset($post_opts['hide_media']) && !$post_opts['hide_media']) ) {
                $args = array(
                    'post_type'         => 'attachment',
                    'post_status'       => 'inherit',
                    'posts_per_page'    => -1,
                    'post_parent'       => $post->ID,
                    'post_mime_type'    => 'image',
                    'orderby'           => 'menu_order',
                    'order'             => 'ASC'
                );
                if( $post_opts['hide_thumbnail'] )
                    $args['post__not_in'] = array( get_post_thumbnail_id() );

                $dt_tmp_query->query( $args );
                if( $dt_tmp_query->have_posts() ) {
                    $slides = array();
                    foreach( $dt_tmp_query->posts as $slide ) {
                        $video = get_post_meta( $slide->ID, '_dt_portfolio_video_link', true );
                        $tmp_arr = array();

                        $tmp_arr['caption'] = $slide->post_excerpt;
                        if( !$video ) {
                            $slide_src = dt_get_resized_img( wp_get_attachment_image_src($slide->ID, 'full'), array('w' => 702) );
                            $tmp_arr['src'] = $slide_src[0];
                            $tmp_arr['size_str'] = $slide_src[3];
                        }else {
                            $tmp_arr['is_video'] = true; 
                            $tmp_arr['src'] = $video; 
                            $tmp_arr['size_str'] = array( 702, 1024 );
                        }

                        $slides[] = $tmp_arr;
                    }
                    dt_get_anything_slider( array('items_arr' => $slides) );
                } ?>

            <div class="full-left">

<?php       } ?>
                    
                <?php
                the_content();
                if( !$post_opts['hide_meta'] ) {
                    dt_get_taxonomy_link(
                        'dt_portfolio_category',
                        '<p>' . __('Category: ', LANGUAGE_ZONE) . '%CAT_LIST%</p>'
                    );
                }
                ?>
            
            <?php if( !isset($post_opts['hide_media']) || (isset($post_opts['hide_media']) && !$post_opts['hide_media']) ): ?>
            </div>
            <?php endif; ?>
            
            <?php
            else:
                echo get_the_password_form();
            endif;// post protected
            endwhile; endif;//have posts ?>
        
        <p class="gap"></p>

        <?php
        $rel_works = get_post_meta($post->ID, '_dt_portfolio_related', true);
        if( isset($rel_works['show_related']) && $rel_works['show_related'] && !post_password_required() ):
        ?>
        
        <p class="hr hr-narrow gap-small"></p>
               
        <div class="gap"></div>
        <div class="full-width w-photo">
            <h2><?php _e('Related Projects', LANGUAGE_ZONE); ?></h2>
            
            <?php
            if( 'same' == $rel_works['related'] ) {
                $rel_works['related'] = wp_get_post_terms(
                    $post->ID,
                    'dt_portfolio_category',
                    array('fields' => 'ids')
                );
            }
            $dt_tmp_query->query( array(
                'posts_per_page'    => -1,
                'post_type'         => 'dt_portfolio',
                'post_status'       => 'publish',
                'post__not_in'      => array($post->ID),
                'tax_query'         => array( array(
                    'taxonomy'  => 'dt_portfolio_category',
                    'field'     => 'id',
                    'terms'     => $rel_works['related'],
                    'operator'  => 'IN'
                ) )    
            ) );
            if( $dt_tmp_query->have_posts() ) {
                $thumb_arr = dt_core_get_posts_thumbnails( $dt_tmp_query->posts );
                $items = array();
                foreach( $dt_tmp_query->posts as $rel_post ) {
                    $item = array();
                    $img = dt_get_resized_img(
                        dt_get_thumb_meta($thumb_arr['thumbs_meta'], 'full', $rel_post->ID),
                        array('w' => 217, 'h' => 122)
                    );
                    $item['src'] = $img[0];
                    $item['size_str'] = $img[3];
                    $item['post_id'] = $rel_post->ID;
                    $items[] = $item;
                }
                dt_get_carousel_slider( array('items_arr' => $items, 'id' => 'foo1') );
            }
            ?>
    
        </div>

        <?php endif; ?>

        <?php comments_template(); ?>

        </div><!-- #container -->
    
    </div><!-- #wrapper -->

<?php get_footer(); ?>
