<?php
/* Template Name: 15. Homepage with Blog */
dt_storage('is_homepage', 'true');
dt_storage('have_sidebar', true);
?>
<?php get_header(); ?>

    <?php get_template_part('top-bg'); ?>

    <?php
    global $paged;
    if( $paged > 1 )
        dt_storage('is_homepage', false);
    
    get_template_part('parallax');
    ?>

    <?php get_template_part('nav'); ?>

    <?php get_template_part('slider'); ?>

    <div id="wrapper">
        
        <?php get_sidebar( 'left' ); ?>

        <div id="container">
            
            <?php
            do_action('dt_layout_before_loop', 'dt-blog');
            global $DT_QUERY;
            if( $DT_QUERY->have_posts() ) {
                while( $DT_QUERY->have_posts() ) {
                    $DT_QUERY->the_post();
                    get_template_part('content', get_post_format() );
                }

	            if( function_exists('wp_pagenavi') ) {
                    wp_pagenavi( $DT_QUERY );
	            }
            }
            wp_reset_postdata();
            ?>

        </div>
        
        <?php get_sidebar( 'right' ); ?>

    </div>

<?php get_footer(); ?>
