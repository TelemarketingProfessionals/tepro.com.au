<?php
/* Template Name: 03. Full-width Page (Default) */
?>
<?php get_header(); ?>

<?php get_template_part('top-bg'); ?>

<?php get_template_part('parallax'); ?>

<?php get_template_part('nav'); ?>

<div id="container" class="full-width">

<?php
if( have_posts() ) {
    while( have_posts() ) { the_post(); the_content(); }
}
?>

</div>

<?php get_footer(); ?>
