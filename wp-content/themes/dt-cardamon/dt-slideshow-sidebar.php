<?php
/* Template Name: 02. Page with Slideshow and Sidebar */
dt_storage('is_homepage', 'true');
dt_storage('have_sidebar', true);
?>
<?php get_header(); ?>

    <?php get_template_part('top-bg'); ?>

    <?php get_template_part('parallax'); ?>

    <?php get_template_part('nav'); ?>

    <?php get_template_part('slider'); ?>

    <div id="wrapper">
        
        <?php get_sidebar( 'left' ); ?>

        <div id="container"><?php the_content(); ?></div>
        
        <?php get_sidebar( 'right' ); ?>

    </div>

<?php get_footer(); ?>
