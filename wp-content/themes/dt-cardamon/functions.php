<?php
require_once dirname(__FILE__) . '/functions/core/core-init.php';

function dt_top_menu_register() {
	register_nav_menu( 'top-menu', __( 'Top Menu', LANGUAGE_ZONE ) );
}
add_action('after_setup_theme', 'dt_top_menu_register');

function dt_setup_scripts() {
    $uri = get_template_directory_uri();

    wp_enqueue_script( 'cufon-yui', $uri.'/js/cufon-yui.js', array('jquery') );
    wp_enqueue_script( 'cufon-colors-origami', $uri.'/js/cufon-colors-origami.js', array('jquery') );
    wp_enqueue_script( 'dt_jquery-easing', $uri.'/js/jquery.easing.1.3.js', array('jquery'), '1.3' );
    //wp_enqueue_script( 'dt_jquery-bxSlider', $uri.'/js/jquery.bxSlider.min.js', array('jquery') );
    wp_enqueue_script( 'dt_jquery-codaSlider', $uri.'/js/jquery.coda-slider-2.0.js', array('jquery'), '2.0' );
    wp_enqueue_script( 'dt_jquery-hs-full', $uri.'/js/plugins/highslide/highslide-full.js', array('jquery') );
    wp_enqueue_script( 'dt_jquery-hs-config', $uri.'/js/plugins/highslide/highslide.config.js', array('jquery', 'dt_jquery-hs-full') );
    wp_enqueue_script( 'dt_scripts', $uri.'/js/scripts.js', array('jquery') );
    wp_enqueue_script( 'dt_custom', $uri.'/js/custom.js', array('jquery') );
    wp_enqueue_script( 'dt_jquery-nivo', $uri.'/js/jquery.nivo.ext.js', array('jquery') );
    wp_enqueue_script( 'dt_sliderman', $uri.'/js/sliderman.1.3.6.js', array('jquery'), '1.3.6' );
    wp_enqueue_script( 'dt_jquery-jparallax', $uri.'/js/jquery.jparallax.js', array('jquery') );
    wp_enqueue_script( 'dt_jquery-event', $uri.'/js/jquery.event.frame.js', array('jquery') );
    wp_enqueue_script( 'dt_anything-slider', $uri.'/js/jquery.anythingslider.js', array('jquery') );
    wp_enqueue_script( 'dt_accordion', $uri.'/js/jquery.accordion.js', array('jquery') );
    wp_enqueue_script( 'dt_organictabs', $uri.'/js/organictabs.jquery.js', array('jquery') );
    wp_enqueue_script( 'dt_shortcodes', $uri.'/js/shortcodes.js', array('jquery', 'dt_accordion') );
    wp_enqueue_script( 'dt_carouFredSel', $uri.'/js/jquery.carouFredSel-4.5.1.js', array('jquery'), '4.5.1' );
    wp_enqueue_script( 'dt_carouselHome', $uri.'/js/jquery.featureCarousel.js', array('jquery') );
    wp_enqueue_script( 'dt_validation', $uri.'/js/plugins/validator/jquery.validationEngine.js', array('jquery') );
    wp_enqueue_script( 'dt_validation_translation', $uri.'/js/plugins/validator/z.trans.en.js', array('jquery') );
    
    if( function_exists('of_get_option') && of_get_option('fonts-enable_cufon', true) ){
		// get font selected in selec
		$font_select = get_template_directory_uri() . of_get_option( 'fonts-list' );
		// if custom upload checked
		if( of_get_option( 'fonts-upload', false ) ){
			// get font from uploder
			$font_upload = site_url() . of_get_option( 'fonts-custom' );
			// if font from uploader exists and hafe .js in its path
			if( $font_upload && strpos($font_upload, '.js') ){
				// add upladed font
				wp_enqueue_script( 'cufon-font', $font_upload );
			}else{
				// add font from select
				wp_enqueue_script( 'cufon-font', $font_select );
			}
		}else{
			// add font from select
			wp_enqueue_script( 'cufon-font', $font_select );
		}
    }elseif( !function_exists('of_get_option') ) {
        wp_enqueue_script( 'cufon-font', get_template_directory_uri() . '/js/fonts/Pacifico_400.font.js' );
    }

    $page_layout = dt_core_get_template_name();
    if( !$page_layout ) {
        $page_layout = 'index.php';
    }else {
        $page_layout = str_replace( array('dt-', '.php', '-sidebar', '-fullwidth'), '', $page_layout ); 
    }

    global $post;
	
	// add some support for qTranslate
	$ajaxurl = admin_url( 'admin-ajax.php' );
	if ( defined( 'QT_SUPPORTED_WP_VERSION' ) ) {
		$ajaxurl = add_query_arg( array( 'lang' => qtrans_getLanguage() ), $ajaxurl );
	}

    $data = array(
	    'ajaxurl'	    => $ajaxurl,
        'post_id'       => isset($post->ID)?$post->ID:'',
        'page_layout'   => $page_layout,
        'nonce'         => wp_create_nonce('nonce_'.$page_layout)
    );

    switch( $page_layout ) {
        case 'portfolio':
            $opts = get_post_meta($post->ID, '_dt_portfolio_layout_options', true);
            break;
        case 'photos':
            $opts = get_post_meta($post->ID, '_dt_photos_layout_options', true);
            break;
        case 'videogal':
            $opts = get_post_meta($post->ID, '_dt_video_layout_options', true);
            break;
        case 'photogallery':
            $opts = get_post_meta($post->ID, '_dt_gallery_layout_options', true);
            break;
        case 'category':
            $opts = get_post_meta($post->ID, '_dt_category_layout_options', true);
            break;
        case 'albums':
            $opts = get_post_meta($post->ID, '_dt_albums_layout_options', true);
            break;
    }
    if( isset($opts['layout']) ) {
        $data['layout'] = end(explode('-', $opts['layout']));
    }

    wp_localize_script( 'dt_scripts', 'dt_ajax', $data );
}
add_action('wp_enqueue_scripts', 'dt_setup_scripts');

function dt_setup_styles() {
    $uri = get_template_directory_uri();

    wp_enqueue_style( 'dt_style',  $uri.'/css/style.css' );
    wp_enqueue_style( 'dt_customize-origami',  $uri.'/css/customize-origami.css' );
    wp_enqueue_style( 'dt_slider',  $uri.'/css/slider.css' );
    wp_enqueue_style( 'dt_slider-nivo',  $uri.'/css/nivo-slider.css' );
    wp_enqueue_style( 'dt_hs',  $uri.'/js/plugins/highslide/highslide.css' );
    wp_enqueue_style( 'dt_anything-slider',  $uri.'/css/anythingslider.css' );
    wp_enqueue_style( 'dt_validation', $uri.'/js/plugins/validator/validationEngine_style.css' );
    wp_enqueue_style( 'dt_shortcodes', $uri.'/css/shortcodes.css' );
    wp_enqueue_style( 'dt_featured_carousel', $uri.'/css/feature-carousel.css' );
}
add_action( 'wp_enqueue_scripts', 'dt_setup_styles' );

function dt_explorer_stuff() {
	if( !is_admin() ):
?>
    <!--[if IE]><script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script><![endif]-->
    <!--[if lte IE 7]><link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/old_ie.css" /><![endif]-->
    <!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/js/plugins/highslide/highslide-ie6.css" />
	<![endif]-->
    <!--[if lte IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/origami-ie8.css" />
	<![endif]-->
<?php
	endif;
}
add_action( 'wp_print_scripts', 'dt_explorer_stuff' );

function dt_setup_admin_scripts( $hook ) {
    if( 'widgets.php' != $hook )
       return; 
    
    wp_enqueue_script( 'dt_admin_widgets', get_template_directory_uri().'/js/admin/admin_widgets_page.js', array('jquery') );
}
add_action("admin_enqueue_scripts", 'dt_setup_admin_scripts');

function dt_footer_widgetarea() {
    global $post;
    if( !empty($post) && is_single() ) {
        switch( $post->post_type ) {
            case 'post':
                dt_widget_area('', null, 'sidebar_5'); return false;
            case 'dt_catalog': 
                dt_widget_area('', null, 'sidebar_6'); return false;
        }
    }

    dt_widget_area('footer');
}

function dt_aside_widgetarea() {
    dt_widget_area('sidebar');
}

function dt_widgets_params( $params ) {
    $params['before_widget'] = '<div class="widget">';
    $params['after_widget'] = '</div>';
    $params['before_title'] = '<div class="header">';
    $params['after_title'] = '</div>';
    return $params;
}
add_filter('dt_setup_widgets_params', 'dt_widgets_params');

function dt_page_navi_args_filter( $args ) {
    $args['wrap'] = '<div id="nav-above" class="navigation blog"><ul class="%CLASS%">%LIST%';

    $add_data = dt_storage('add_data');
    if( $add_data &&
        isset($add_data['init_layout']) &&
        $add_data['init_layout'] == '3_col-list'
    ) {
        $args['wrap'] = '<div id="nav-above" class="navigation blog with-3-col"><ul class="%CLASS%">%LIST%';
    }

    $args['item_wrap'] = '<li class="%ACT_CLASS%"><a href="%HREF%" class="button"><span>%TEXT%</span></a></li>';
    $args['first_wrap'] = '<li class="larr"><a href="%HREF%" class="button"><span>%TEXT%</span></a></li>';
    $args['last_wrap'] = '<li class="rarr"><a href="%HREF%" class="button"><span>%TEXT%</span></a></li>';
    $args['dotleft_wrap'] = '<li class="dotes">%TEXT%</li>'; 
    $args['dotright_wrap'] = '<li class="dotes">%TEXT%</li>';
    $args['pages_wrap'] = '</ul><div class="paginator-r"><span class="pagin-info">%TEXT%</span>%PREV%%NEXT%</div></div>';
    $args['pages_prev_class'] = 'prev';
    $args['pages_next_class'] = 'next';
    $args['act_class'] = 'act';
    return $args;
}
add_filter('wp_page_navi_args', 'dt_page_navi_args_filter');

function dt_details_link( $post_id = null, $class = 'button' ) {
    if( empty($post_id) ) {
        global $post;
        $post_id = $post->ID;
    }
	
	if ( 'page' == get_post_type( $post_id ) ) { return false; }
    
	$url = get_permalink($post_id);
    if( $url ) {
        printf(
            '<a href="%s" class="%s"><span><i class="more"></i>%s</span></a>',
            $url, $class, __('Details', LANGUAGE_ZONE)
        );
    }
}

// edit post link
function dt_edit_link( $text = 'Edit', $post_id = null, $class = 'button' ) {
    if( current_user_can('edit_posts')) {
        global $post;
        if( empty($post_id) && $post ) {
            $post_id = $post->ID;
        }
        
        if( !empty($class) )
            $class = sprintf( ' class="%s"', esc_attr($class) );

        printf( '<a href="%s"' . $class . ' style="margin-left: 5px;"><span>%s</span></a>',
            get_edit_post_link($post_id),
            $text
        );
    }
}

/* comments callback
 */
function dt_single_comments( $comment, $args, $depth ) {
    static $comments_count = 0, $prev_depth = 1;
    $comments_count++;
     
    if( $prev_depth != $depth ) {
        // this is shit 
        if( $depth < $prev_depth ) {
            for( $i = $depth; $i < $prev_depth; $i++ ) {
                echo '</div>';
            }
        }
        $prev_depth = $depth;
    }
     
    $classes = array();
    if( !$args['has_children'] || ($depth == 5) ) {
        $classes[] = 'nochildren';
    }

    $GLOBALS['comment'] = $comment;
    $avatar_size = 52;
    $classes = implode( ' ', $classes ); 
    ?>
    <div id="comment-<?php echo $comment->comment_ID ?>" class="comment level<?php echo $depth; ?> <?php echo esc_attr($classes); ?>">
        <span class="avatar"><?php
            echo get_avatar(
                $comment,
                $avatar_size,
                esc_url( get_template_directory_uri() . '/images/com.jpg' )
            );
        ?></span>
          <span class="text<?php echo ($args['comments_nmbr'] == $comments_count)?' last':''; ?>"><span class="head"><?php comment_author($comment->comment_ID); ?></span><span class="comment-meta"><span class="ico-link date"><?php
                printf(
                    __('%s at %s', LANGUAGE_ZONE ),
                    get_comment_date(),
                    get_comment_time()
                );
                ?></span><?php
        edit_comment_link(
            __( 'Edit', LANGUAGE_ZONE ),
            '<span class="edit-link">',
            '</span>'
        );
        ?><a href="#" class="ico-link comments"><?php _e( 'Reply', LANGUAGE_ZONE ); ?></a></span>
        <?php comment_text(); ?>
        </span>
    </div><!-- close element -->
    <?php
    
    if( $depth >= 1 && $depth < 5 && $args['has_children'] ) {
        echo '<div class="children">';
    }
    
    if( $args['comments_nmbr'] == $comments_count ) {
        for( $i = 0; $i < $depth - 1; $i++ ) {
            echo '</div>';
        }
    }

}

function dt_comments_end_callback() {
}

// gravatar
/*
add_filter( 'avatar_defaults', 'dt_default_avatar' );
function dt_default_avatar ( $avatar_defaults ) {
		$new_avatar_url = get_bloginfo( 'template_directory' ) . '/images/com.jpg';
		$avatar_defaults[$new_avatar_url] = 'DT Default avatar';
		return $avatar_defaults;
}
 */
function dt_index_layout_init( $layout ) {
    if( 'index' != $layout ) {
        return false;
    }

    
    if( 'post-format-standard' == get_query_var('post_format') ) {
        global $_wp_theme_features;
        $pf_arr = array();
        foreach( $_wp_theme_features['post-formats'][0] as $pf ){
            $pf_arr[] = 'post-format-' . $pf;
        }
        
        if( !$paged = get_query_var('page') )
            $paged = get_query_var('paged');

        query_posts( array(
            'tax_query' => array( array(
                'taxonomy'  => 'post_format',
                'field'     => 'slug',
                'terms'     => $pf_arr,
                'operator'  => 'NOT IN'
            ) ),
            'post_type'     => 'post',
            'paged'                 => $paged,
            'post_status'           => 'publish',
            'ignore_sticky_posts'   => true
        ) );
    }

    global $wp_query;
    if( have_posts() ) {
        $thumb_arr = dt_core_get_posts_thumbnails( $wp_query->posts );
        dt_storage( 'thumbs_array', $thumb_arr['thumbs_meta'] );
    }  
    dt_storage( 'post_is_first', 1 );
}
add_action('dt_layout_before_loop', 'dt_index_layout_init', 10, 1);

function dt_main_block_class_changer( $class = '', $echo = true ) {
    $template = dt_core_get_template_name();
    $classes = array();
    
    if( $class ) {
        $classes[] = $class;
    }

    switch( $template ) {
        case 'dt-homepage-blog.php':
            global $paged;
            if( $paged > 1) {
                $classes[] = 'bg';
                break;
            }
        case 'dt-slideshow-fullwidth.php':
        case 'dt-slideshow-sidebar.php':
            $classes[] = 'home-bg';
            break;    
        default:
            $classes[] = 'bg';
    }
    
    if( $echo ) {
        echo implode(' ', $classes);
    }else {
        return $classes;
    }
    return false;
}

function dt_get_layout_switcher( $opts = array(), $echo = true ) {
    $buttons = array(
        '2_col'     => array(
            'grid'  => array(
                'class'     => 'categ td',
                'href'      => 'grid',
                'i_class'   => 'ico-f'
            ),
            'list' => array(
                'class'     => 'categ list',
                'href'      => 'list',
                'i_class'   => 'ico-t'
            )
        ),
        '3_col'     => array(
            'grid'  => array(
                'class'     => 'categ td-three',
                'href'      => 'grid',
                'i_class'   => 'three-coll'
            ),
            'list' => array(
                'class'     => 'categ list-three',
                'href'      => 'list',
                'i_class'   => 'three-coll-t'
            )
        )
    );
    $defaults = array(
        'type'      => '2_col',
        'current'   => 'list',
        'hash'      => '%s'
    );
    $opts = wp_parse_args( $opts, $defaults );

    if( !isset($buttons[$opts['type']]) || !isset($buttons[$opts['type']][$opts['current']]) ) {
        return false;
    }

    $output = '';

    foreach( $buttons[$opts['type']] as $select=>$button ) {
        if( $select == $opts['current'] ) {
            $button['class'] .= ' act';
        }
        $output .= sprintf(
            '<a class="button %s" href="%s"><span><i class="%s"></i></span></a>',
            $button['class'],
            sprintf( $opts['hash'], $button['href'] ),
            $button['i_class']
        );
    }

    if( $echo ) {
        echo $output;
    }else {
        return $output;
    }
    return false;
}

function dt_excerpt_more_filter( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'dt_excerpt_more_filter' );

function dt_get_category_list_options_filter( $opts ) {
    $opts['wrap'] = '%LIST%';
    $opts['item_wrap'] = '<a href="%HREF%" class="%CLASS%"><span>%TERM_NICENAME% (%COUNT%)</span></a>';
    $opts['item_class'] = 'button';
    return $opts;
}
add_filter( 'dt_get_category_list_options', 'dt_get_category_list_options_filter' );

function dt_category_list( array $opts ) {
    $defaults = array(
        'taxonomy'          => null,
        'post_type'         => null,
        'layout'            => null,
        'terms'             => array(),
        'select'            => 'all',
        'layout_switcher'   => true,
        'count_attachments' => false,
        'show'              => true,
        'post_ids'          => array()
    );
    $opts = wp_parse_args( $opts, $defaults );
    
    if( !($opts['taxonomy'] && $opts['post_type'] && $opts['layout'] && ($opts['show'] || $opts['layout_switcher'])) ) {
        return '';
    }
    
    if( $opts['show'] || $opts['layout_switcher'] ):

        $layout = explode('-', $opts['layout']);

        if( $opts['show'] ) {
            $list = dt_get_category_list( array(
                'taxonomy'          => $opts['taxonomy'],
                'post_type'         => $opts['post_type'],
                'terms'             => $opts['terms'],
                'count_attachments' => $opts['count_attachments'],
                'select'            => $opts['select'],
                'post_ids'          => $opts['post_ids'],
                'hash'              => '#%TERM_ID%/%PAGE%/' . (isset($layout[1])?$layout[1]:'list')
            ), false );
        }else
            $list = false;
    ?>
    
<div class="navig-category<?php echo !$list?' no-category':''; ?>">
    
    <?php
    
    echo $list;

    if( isset($layout[1]) && $opts['layout_switcher'] ) {
        dt_get_layout_switcher( array(
            'type'      => $layout[0],
            'current'   => $layout[1],
            'hash'      => '#all/1/%s'
        ) );
    }
    ?>

</div>

    <?php
    endif;
}

function dt_post_type_do_ajax() {
    $new_paged = !empty($_POST['paged'])?trim(intval($_POST['paged'])):1;
    $layout = !empty($_POST['layout'])?trim(stripslashes($_POST['layout'])):''; 

    $cat_id = !empty($_POST['cat_id'])?trim(stripslashes($_POST['cat_id'])):'all';
    if( 'all' != $cat_id && 'none' != $cat_id ) {
       $cat_id = explode(',', $cat_id); 
    }

    if( isset($_POST['page_layout']) ) {
        $page_layout = trim(stripslashes($_POST['page_layout']));
    }else {
        wp_die( __('Empty page layout', LANGUAGE_ZONE) );
    }

    if( !empty($_POST['post_id']) ) {
        $post_id = intval(trim($_POST['post_id']));
    }else {
        wp_die( __('Empty post id', LANGUAGE_ZONE) );
    }

    switch( $page_layout ) {
        case 'portfolio':
            $page_layout = 'dt-portfolio';
            break;
        case 'photos':
            $page_layout = 'dt-photos';
            break;
        case 'catalog':
            $page_layout = 'dt-catalog';
            break;
        case 'albums':
            $page_layout = 'dt-albums';
            break;
        case 'videogal':
            $page_layout = 'dt-video';
            break;
        default:
            wp_die( __('Undefined page layout', LANGUAGE_ZONE) );
    }

    // do page init
    global $wp_query;
    $wp_query->query('page_id=' . $post_id . '&status=publish');

    if( have_posts() ) {
        the_post();
    }else {
        wp_die( __('There are no such page', LANGUAGE_ZONE) );
    }

    // replace paged
    $wp_query->set('paged', $new_paged);
    global $paged;
    $paged = $new_paged;

    // store settings
    dt_storage( 'page_data', array(
        'cat_id'        => is_array($cat_id)?$cat_id:array($cat_id),
        'page_layout'   => $page_layout,
        'base_url'      => get_permalink($post_id),
        'layout'        => $layout
    ) );
    
    $data = array(
        'cat_id'    => $cat_id    
    );

    do_action('dt_layout_before_loop', $page_layout, $data );
    global $DT_QUERY;
    if( $DT_QUERY->have_posts() ) {

        while( $DT_QUERY->have_posts() ) {
            $DT_QUERY->the_post();
            get_template_part('content', $page_layout);
        }
        if( function_exists('wp_pagenavi') ) {
            wp_pagenavi( $DT_QUERY, array( 'ajaxing'   => true, 'num_pages' => dt_storage('num_pages', null, 5)) );
        }
    }

    // IMPORTANT: don't forget to "exit"
    exit;
}
add_action( 'wp_ajax_nopriv_dt_post_type_do_ajax', 'dt_post_type_do_ajax' );
add_action( 'wp_ajax_dt_post_type_do_ajax', 'dt_post_type_do_ajax' );

function dt_get_anything_slider( $opts = array(), $echo = true ) {
    $defaults = array(
        'wrap'      => '<div class="%CLASS%">%SLIDER%</div>',
        'class'     => 'slider-shortcode anything gal',
        'items_arr' => array()
    );
    $opts = wp_parse_args( $opts, $defaults );
    
    if( empty($opts['items_arr']) )
        return '';

    $output = '';
    foreach( $opts['items_arr'] as $slide ) {
        $caption = '';
        if( !empty($slide['caption']) ) {
            $caption = sprintf(
                '<span class="html-caption"><p>%s</p></span>',
                $slide['caption']
            );
        }

        $output .= '<li class="panel">'."\n";
        if( !isset($slide['is_video']) || $slide['is_video'] == false ) {
            $output .= sprintf(
                '<img src="%s" %s />%s',
                $slide['src'],
                $slide['size_str'],
                $caption
            );
        }else {
            $output .= dt_get_embed( $slide['src'], $slide['size_str'][0], $slide['size_str'][1], false );
        }
        $output .= '</li>';
    }
    $output = '<ul class="anything-slider">' . $output . '</ul>';
    
    $output = str_replace( array(
            '%SLIDER%',
            '%CLASS%'
        ), array(
            $output,
            $opts['class']
        ), $opts['wrap']
    );

    if( $echo ) {
        echo $output;
    }else {
        return $output;
    }
    return false;
}

function dt_get_carousel_slider( array $opts = array(), $echo = true ) {
    $defaults = array(
        'wrap'      => '<div class="%CLASS%">%SLIDER%<a id="prev1" class="prev" href="#"></a><a id="next1" class="next" href="#"></a></div>',
        'class'     => 'list-carousel recent',
        'id'        => '',
        'items_arr' => array()
    );
    $opts = wp_parse_args( $opts, $defaults );
    
    $output = '';
    foreach( $opts['items_arr'] as $item ) {
        $caption = '';
/*        if( $slide['caption'] ) {
            $caption = sprintf(
                '<span class="html-caption">%s</span>',
                $slide['caption']
            );
        }
 */     
        if( !isset($item['href']) ) {
            if( isset($item['post_id']) ) {
                $item['href'] = get_permalink($item['post_id']);
            }else {
                $item['href'] = '#';
            }
        }
        $output .= sprintf(
            '<li><div class="textwidget"><div class="textwidget-photo"><a class="photo" href="%s"><img src="%s" %s /></a></div></div></li>',
            $item['href'],
            $item['src'],
            $item['size_str']
//            $item['caption']
        );
    }
    $output = '<ul' . ( !empty($opts['id'])?' id="' . $opts['id'] . '"':'' ) . ' class="carouFredSel_1">' . $output . '</ul>';
    
    $output = str_replace( array(
            '%SLIDER%',
            '%CLASS%'
        ), array(
            $output,
            $opts['class']
        ), $opts['wrap']
    );

    if( $echo ) {
        echo $output;
    }else {
        return $output;
    }
    return false;
}

function dt_get_carousel_homepage_slider( array $opts = array(), $echo = true ) {
    $defaults = array(
        'wrap'      => '<div class="navig-nivo caros"><div id="carousel-left"></div><div id="carousel-right"></div></div><section id="%ID%"><div id="carousel-container"><div id="carousel">%SLIDER%</div></div></section>',
        'class'     => '',
        'id'        => 'slide',
        'items_arr' => array()
    );
    $opts = wp_parse_args( $opts, $defaults );
    
    $output = '';
    foreach( $opts['items_arr'] as $item ) {
        $item_output = '';
        $link = '<a href="javascript: void(0);">%s</a>';
		
        if( !empty($item['title']) )
            $item_output .= '<div class="caption-head">' . $item['title'] . '</div>';

        if( !empty($item['caption']) )
            $item_output .= '<div class="text-capt">' . $item['caption'] . '</div>';

        if( $item_output )
            $item_output = '<div class="carousel-caption">' . $item_output . '</div>';
		
		if( !empty($item['link']) ) {
			if( !empty($item['in_neww']) )
				$link = '<a href="'. $item['link']. '" target="_blank">%s</a>';
			else
				$link = '<a href="'. $item['link']. '">%s</a>';
		}
		
		$img = sprintf( $link, '<img class="carousel-image" alt="' . ( !empty($item['title'])?esc_attr($item['title']):'' ) . '" src="' . $item['src'] . '" />' );
        $item_output = $img. '<div class="mask"><img alt="" src="' . get_template_directory_uri() . '/images/bg-carousel.png" /></div>' . $item_output;

        $output .= '<div class="carousel-feature">' . $item_output . '</div>';
    }

    $output = str_replace(
        array( '%SLIDER%', '%CLASS%', '%ID%' ),
        array( $output, $opts['class'], $opts['id'] ),
        $opts['wrap']
    );

    if( $echo ) {
        echo $output;
    }else {
        return $output;
    }
    return false;
}

function dt_get_photo_stack_slider( array $opts = array(), $echo = true ) {
    wp_enqueue_script( 'dt_photo_stack', get_template_directory_uri().'/js/photo-stack.js', array('jquery') );

    $defaults = array(
        'wrap'      => '<div class="navig-nivo ps"><a class="prev"></a><a class="next"></a></div><section id="%ID%"><div id="ps-slider" class="ps-slider"><div id="ps-albums">%SLIDER%</div></div></section>',
        'class'     => '',
        'id'        => 'slide',
        'items_arr' => array()
    );
    $opts = wp_parse_args( $opts, $defaults );
    
    $output = '';
    foreach( $opts['items_arr'] as $item ) {
        $item_output = $link = $link_class = '';
                	
        if( !empty($item['title']) )
            $item_output .= '<div class="ps-head"><h3>' . $item['title'] . '</h3></div>';

        if( !empty($item['caption']) )
            $item_output .= '<div class="ps-cont">' . $item['caption'] . '</div>';

        if( $item_output )
            $item_output = '<div class="ps-desc">' . $item_output . '</div>';
		
		if( !empty($item['link']) ) {
			$link_class = ' dt-clickable';
			if( !empty($item['in_neww']) )
				$link = ' onclick="window.open(\''. $item['link']. '\');"';
			else
				$link = ' onclick="window.location=\''. $item['link']. '\'";';
		}
	
        $item_output = '<img class="carousel-image" alt="' . ( !empty($item['title'])?esc_attr($item['title']):'' ) . '" src="' . $item['src'] . '" />' . $item_output;

        $output .= '<div class="ps-album"><div class="ps-inner'. $link_class. '"'. $link. '>' . $item_output . '</div></div>';
    }

    $output = str_replace(
        array( '%SLIDER%', '%CLASS%', '%ID%' ),
        array( $output, $opts['class'], $opts['id'] ),
        $opts['wrap']
    );

    if( $echo ) {
        echo $output;
    }else {
        return $output;
    }
    return false;
}

function dt_get_jfancy_tile_slider( array $opts = array(), $echo = true ) {
    wp_enqueue_script( 'dt_jfancy_tile', get_template_directory_uri().'/js/jquery.jfancytile.js', array('jquery') );

    $defaults = array(
        'wrap'      => '<div class="navig-nivo"></div><section id="%ID%"><div id="fancytile-slide"><ul>%SLIDER%</ul></div><div class="mask"></div></section>',
        'class'     => '',
        'id'        => 'slide',
        'items_arr' => array()
    );
    $opts = wp_parse_args( $opts, $defaults );
    
    $output = '';
    foreach( $opts['items_arr'] as $item ) {
        $item_output = $link = '';
                	
        if( !empty($item['title']) )
            $item_output .= '<div class="caption-head">' . $item['title'] . '</div>';

        if( !empty($item['caption']) )
            $item_output .= '<div class="text-capt">' . $item['caption'] . '</div>';

        if( $item_output )
            $item_output = '<div class="html-caption">' . $item_output . '</div>';
		
		if( !empty($item['link']) ) {
			$link = ' data-link="'. esc_attr($item['link']). '"';
			if( isset($item['in_neww']) )
				$link .= ' data-target_blank="'. intval($item['in_neww']). '"';
		}
		
        $item_output = '<img alt="' . ( !empty($item['title'])?esc_attr($item['title']):'' ) . '" src="' . $item['src'] . '" ' . $item['size_str'] . $link. ' />' . $item_output;

        $output .= '<li>' . $item_output . '</li>';
    }

    $output = str_replace(
        array( '%SLIDER%', '%CLASS%', '%ID%' ),
        array( $output, $opts['class'], $opts['id'] ),
        $opts['wrap']
    );

    if( $echo ) {
        echo $output;
    }else {
        return $output;
    }
    return false;
}

function dt_get_coda_slider( array $opts = array() ) {
    $defaults = array(
        'wrap'      => '<div class="coda-slider-wrapper"><div class="coda-slider preload">%SLIDER%</div></div>',
        'item_wrap' => '<div class="panel"><div class="panel-wrapper">%1$s</div><div class="panel-author">%2$s</div></div>',
        'data'      => array(),
        'wrap_data' => array(),
        'echo'      => true
    );
    $opts = wp_parse_args( $opts, $defaults );
    
    if( empty($opts['data']) || !is_array($opts['data']) ) {
        return '';
    }

    $output = '';
    foreach( $opts['data'] as $slide ) {
        if( !is_array($slide) ) {
            continue;
        }

//        $slide[0] = apply_filters('the_content', strip_shortcodes($slide[0]));
        
        $replace_arr = array();
        for( $i = 0; $i < count( $slide ); $i++ ) {
            $replace_arr[] = '%' . ($i + 1) . '$s';
            if( isset($opts['wrap_data'][$i]) ) {
                $slide[$i] = sprintf( $opts['wrap_data'][$i], $slide[$i] );
            }
        }
        $output .= str_replace( $replace_arr, $slide, $opts['item_wrap'] );
    }
    
    $output = str_replace( array('%SLIDER%'), array( $output ), $opts['wrap'] );

    if( $opts['echo'] ) {
        echo $output;
    }else {
        return $output;
    }
    return false;
}

/* Nivo slider helper
 */
function dt_get_nivo_slider( array $opts = array(), $echo = true ) {
    $defaults = array(
        'wrap'          => '<div class="navig-nivo big-slider"><div class="nivo-directionNav"><a class="nivo-prevNav"></a><a class="nivo-nextNav"></a></div></div><section id="%ID%"><div class="%CLASS%">%SLIDER%</div><div class="mask"></div><div class="grid"></div></section>',
        'class'         => 'slider-wrapper theme-default',
        'id'            => 'slide',
        'items_wrap'    => '<div id="slider" class="nivoSlider">%IMAGES%</div>%CAPTIONS%',
        'items_arr'     => array()
    );
    $opts = wp_parse_args( $opts, $defaults );

    if( empty($opts['items_arr']) )
        return '';

    $output = '';
    $images = $caption = '';
    $i = 1;

    foreach( $opts['items_arr'] as $slide ) {
		$link = '%s';
        $caption .= '<div class="nivo-html-caption caption-' . $i . '">';

        if( !empty($slide['title']) )
            $caption .= '<div class="caption-head">' . $slide['title'] . '</div>';

        if( !empty($slide['caption']) )
            $caption .= '<div class="text-capt">' . $slide['caption'] . '</div>';
		
		if( !empty($slide['link']) ) {
			if( !empty($slide['in_neww']) )
				$link = '<a href="'. $slide['link']. '" target="_blank">%s</a>';
			else
				$link = '<a href="'. $slide['link']. '">%s</a>';
		}
		
        $caption .= '</div>';

        $images .= sprintf( $link, '<img src="' . $slide['src'] . '" alt="" title=".caption-' . $i . '" ' . $slide['size_str'] . ' />' );
        $i++;
    }

    $output .= str_replace( array('%IMAGES%', '%CAPTIONS%'), array($images, $caption), $opts['items_wrap'] );
/*    
    '<div id="slider" class="nivoSlider">' . $images . '</div>' . $caption;
 */
    $output = str_replace(
        array( '%SLIDER%', '%CLASS%', '%ID%' ),
        array( $output, $opts['class'], $opts['id'] ),
        $opts['wrap']
    );

    if( $echo ) {
        echo $output;
    }else {
        return $output;
    }
    return false;
}

function dt_portfolio_classes( $type, $place = '', $echo = true ) {
    if( empty( $place ) ) { return ''; }
	if ( empty( $type ) ) { $type = '2_col-list'; }
	
    $class = array(
        '2_col' => array(
            'list'  => array(
                'fullwidth' => array(
                    'block' => 'textwidget text',
                    'head'  => 'head',
                    'info'  => 'info half'
                ),
                'sidebar'   => array(
                    'block' => 'textwidget text',
                    'head'  => 'head',
                    'info'  => 'info half'
                )
            ),
            'grid'  => array(
                'fullwidth' => array(
                    'block' => 'textwidget',
                    'head'  => 'head',
                    'info'  => 'info half'
                ),
                'sidebar'   => array(
                    'block' => 'textwidget',
                    'head'  => 'head',
                    'info'  => 'info half'
                )
            )
        ),
        '3_col' => array(
            'list'  => array(
                'fullwidth' => array(
                    'block' => 'textwidget one-third',
                    'head'  => 'head-capt',
                    'info'  => 'info one-third'
                ),
                'sidebar'   => array(
                    'block' => 'textwidget one-fourth',
                    'head'  => 'head-capt',
                    'info'  => 'info one-fourth'
                )
            ),
            'grid'  => array(
                'fullwidth' => array(
                    'block' => 'textwidget',
                    'head'  => 'head-capt',
                    'info'  => 'info one-third'
                ),
                'sidebar'   => array(
                    'block' => 'textwidget',
                    'head'  => 'head-capt',
                    'info'  => 'info one-fourth'
                )
            )
        )
    );
	$class = apply_filters( 'dt_portfolio_default_classes', $class, $type, $place );

    $add_data = dt_storage( 'add_data' );

    $type = explode( '-', $type );
	$cols = $type[0];
	$form = $type[1];
    if ( ! isset( $class[ $cols ][ $form ][ $add_data['template_layout'] ][ $place ] ) ) { return ''; }

    $output_class = apply_filters( 'dt_portfolio_classes', $class[ $cols ][ $form ][ $add_data['template_layout'] ][ $place ], $type, $place, $class, $echo );
    
    if( $echo ) {
        echo $output_class;
    }else {
        return $output_class;
    }
    return false;
}

function dt_storage_add_data_init( array $data ) {
//    $opts = get_post_meta($post->ID, '_dt_portfolio_layout_options', true);
    $thumb_sizes = array(
        '2_col' => array(
            'fullwidth' => array( 462, 272 ),
            'sidebar'   => array( 337, 202 )
        ),
        '3_col' => array(
            'fullwidth' => array( 298, 172 ),
            'sidebar'   => array( 215, 122 )
        )
    );
    $add_data = array();
    $template = dt_core_get_template_name();
    $page_data = dt_storage('page_data');

    if( isset($data['layout']) ) {
        $cols_layout = current(explode('-', $data['layout']));
        $add_data['init_layout'] = $cols_layout . '-' . $page_data['layout'];//$opts['layout']; 
    }else {
        $cols_layout = '';
    }
    
    if( isset($data['template_layout']) ) {
        $template_layout = array( '1' => $data['template_layout'] );
    }else {
        $template_layout = explode( '-', str_replace(array('dt-', '.php'), '', $template) );
    }    
    if( isset($template_layout[1]) ) {
        $template_layout = $template_layout[1];
        if( isset($thumb_sizes[$cols_layout][$template_layout][0]) &&
            isset($thumb_sizes[$cols_layout][$template_layout][1])
        ) {
            $add_data['thumb_w'] = $thumb_sizes[$cols_layout][$template_layout][0];
            $add_data['thumb_h'] = $thumb_sizes[$cols_layout][$template_layout][1];
        }
    }else {
        $template_layout = '';
    }

    $add_data['cols_layout'] = $cols_layout;
    $add_data['template_layout'] = $template_layout;
     
    dt_storage('add_data', $add_data);
}

function dt_style_options_get_image( $params, $img_1, $img_2 = '', $use_second_img = false ) {
    if( 'none' == $img_1 && (!$img_2 || 'none' == $img_2) )
        return 'none;';
    
    if( 'none' == $img_1 && !$use_second_img ) {
        return $img_1 . ';';
    }
    
    $defaults = array(
        'repeat'    => 'repeat',
        'x-pos'     => 0,
        'y-pos'     => 0,
        'important' => true
    );
    $params = wp_parse_args( $params, $defaults );

    $output = get_stylesheet_directory_uri() . $img_1;
    if( $use_second_img && $img_2 ) {
        $output = site_url() . $img_2;
    }
    $output = sprintf(
        'url(%s)%s;',
        esc_url($output),
        ($params['important']?' !important':'')
    );
    return $output;
}

function dt_style_options_get_bg_position( $y, $x ) {
    return sprintf( '%s %s !important;', $y, $x );
}

function dt_style_options_get_rgba_from_hex_color( $params, $color, $opacity = 0 ) {
    $defaults = array(
        'important' => true
    );
    $params = wp_parse_args( $params, $defaults );

    if( is_array($color) ) {
        $rgb_array = array_map('intval', $color);    
    }else {
        $color = str_replace('#', '', $color);
        $rgb_array = str_split($color, 2);
        if( is_array($rgb_array) && count($rgb_array) == 3 ) {
            $rgb_array = array_map('hexdec', $rgb_array);
        }else {
            return '#ffffff;';
        }
    }

    $opacity = ($opacity > 0)?$opacity/100:0;
    
    return sprintf(
        'rgba(%d,%d,%d,%s)%s;',
        $rgb_array[0], $rgb_array[1], $rgb_array[2], $opacity,
        ($params['important']?' !important':'')
    );
}

function dt_style_options_get_rgba_from_hex_color_for_ie( $params, $color, $opacity = 0 ) {
    $defaults = array(
        'important' => true
    );
    $params = wp_parse_args( $params, $defaults );

    if( is_array($color) ) {
        $hex_color = implode( '', $color );   
    }else{
        $hex_color = str_replace( '#', '', $color );
    }

    $opacity = ($opacity > 0)?round($opacity*2.55):0;
    
    
    return sprintf(
        'progid:DXImageTransform.Microsoft.gradient(startColorstr=#%2$s%1$s,endColorstr=#%2$s%1$s)%3$s;',
        $hex_color, dechex($opacity), ($params['important']?' !important':'')
    );
}

function dt_get_shadow_color( $color, $params = ' 1px 1px 0' ) {
	$shadow = 'none';
	if( $color )
		$shadow = $color. $params;
	return $shadow;
}

function dt_get_soc_links() {
    $links = of_get_option('social_icons');

    if( empty($links) ) {
        return '';
    }

?>

    <ul class="soc-ico">

		<?php
		foreach( $links as $name=>$data ):
			
			if ( 'skype' != $name ) {
				$href = esc_url( $data['link'] );
			} else {
				$href = esc_attr( $data['link'] );
			}
			
		?>

        <li><a class="<?php echo $name; ?> trigger" href="<?php echo $href; ?>" target="_blank"><span><?php echo $name; ?></span></a></li> 

		<?php endforeach; ?>

    </ul>

<?php
}

function dt_style_options() {
    global $post;
    ?>
    <style type="text/css">
    
    /* appearance logo */
/*    .logo {
        text-align: <?php echo esc_attr(of_get_option('appearance-header_logo_position', 'center')); ?> ;
    }
 */   
    /* appearance footer logo */
    .logo-down {
<?php
/*
    switch( of_get_option('appearance-footer_logo_position', false) ) {
        case 'right': ?>
        float: right !important;       
<?php       break;
        case 'left': ?>
        float: left !important;    
<?php       break;
        default: ?>
        margin-left: auto !important;
        margin-right: auto !important;
        float: left !important;             
<?php
    }
*/
?>
    }

        /* appearance background font-family content-text-color content-text-shadow-color */    
        body {
            background-image: <?php echo dt_style_options_get_image(
                array(),
                of_get_option('background-bg_image', 'none'),
                of_get_option('main_bg-bg_custom'),
                of_get_option('main_bg-bg_upload')
            ); ?>
            background-position: <?php echo dt_style_options_get_bg_position( of_get_option('main_bg-bg_horizontal_pos'), of_get_option('main_bg-bg_vertical_pos') ); ?>
            
			<?php if( of_get_option('main_bg-bg_fixed') ):?>
			background-attachment: fixed;
			<?php endif;?>
			
			background-color: <?php echo of_get_option('main_bg-bg_color'); ?>;
            background-repeat: <?php echo of_get_option('main_bg-bg_repeat'); ?>;
            font: 12px/20px "<?php echo str_replace('_', ' ', of_get_option('fonts-font_family')); ?>", Arial, Helvetica, sans-serif;
            color: <?php echo of_get_option('fonts_content-primary_color'); ?>; 
            text-shadow: <?php echo dt_get_shadow_color( of_get_option('fonts_content-primary_shadow_color') );	?>;
        }
        .pagin-info, .team-wrap .head, .custom-menu li a, .panel .panel-wrapper a, .reviews-t, .post a, ul.categories li a, .do-clear {
            color: <?php echo of_get_option('fonts_content-primary_color'); ?> !important; 
            text-shadow: <?php echo dt_get_shadow_color( of_get_option('fonts_content-primary_shadow_color') );	?> !important;
        }
    
    /* dividers content */
    <?php
    $wide_divider = dt_style_options_get_image(
        array( 'important' => false ),
        of_get_option('divs_and_heads-content_wide_divider', 'none'),
        of_get_option('divs_and_heads-content_wide_divider_custom'),
        of_get_option('divs_and_heads-content_wide_divider_upload')
    );

    $wide_divider_important = dt_style_options_get_image(
        array(),
        of_get_option('divs_and_heads-content_wide_divider', 'none'),
        of_get_option('divs_and_heads-content_wide_divider_custom'),
        of_get_option('divs_and_heads-content_wide_divider_upload')
    );

    $narrow_divider = dt_style_options_get_image(
        array(),
        of_get_option('divs_and_heads-content_narrow_divider', 'none'),
        of_get_option('divs_and_heads-content_narrow_divider_custom'),
        of_get_option('divs_and_heads-content_narrow_divider_upload')
    );
    ?>

        .hr.hr-wide, .entry-content.cont {
            background-image: <?php echo $wide_divider; ?>
            background-position: <?php echo dt_style_options_get_bg_position('top', 'left'); ?>
            background-repeat: <?php echo of_get_option('divs_and_heads-content_wide_divider_repeatx')?'repeat-x':'no-repeat'; ?>;
	    }

        .entry-content.one-line {		
            background-image: <?php echo $wide_divider_important; ?>		
            background-position: <?php echo dt_style_options_get_bg_position('bottom', 'left'); ?>
            background-repeat: <?php echo of_get_option('divs_and_heads-content_wide_divider_repeatx')?'repeat-x':'no-repeat'; ?>;
	    }
			
	    #comments .text, .hr.hr-narrow, .type-post, .post, ul.categories li, .custom-menu li, .gallery .textwidget.text, .item-blog  {
		    background-image: <?php echo $narrow_divider; ?>
            background-position: <?php echo dt_style_options_get_bg_position('top', 'left'); ?>
            background-repeat: <?php echo of_get_option('divs_and_heads-content_narrow_divider_repeatx')?'repeat-x':'no-repeat'; ?>;
	    }
        
	    #comments .text {
            background-position: <?php echo dt_style_options_get_bg_position('bottom', 'left'); ?>
	    }
    /* end dividers content */
    
    /* widgets/shotcodes background */
    <?php
    $color = dt_style_options_get_rgba_from_hex_color(
        array( 'important' => false ),
        of_get_option('widgetcodes-bg_color'),
        of_get_option('widgetcodes-bg_opacity')
    );
    $ie_color = dt_style_options_get_rgba_from_hex_color_for_ie(
        array( 'important' => false ),
        of_get_option('widgetcodes-bg_color'),
        of_get_option('widgetcodes-bg_opacity')
    )
    ?>
        #wp-calendar td, #calendar_wrap, #wp-calendar caption, #aside .slider-wrapper, .half .slider-wrapper, .full .slider-wrapper, .two-thirds .slider-wrapper, .one-fourth .slider-wrapper, #aside .slider_container_1, .slider_container_1 {
            background-color: <?php echo $color; ?>
		}

		.slider-shortcode, .contact-block, .about, .textwidget-photo, .partner-bg, .alignleft, .alignright, .aligncenter, .alignnone, .gallery-item, .shadow-light, .reviews-t, #aside .twit .reviews-t, .blockquote-bg, .navigation, .map, .navig-category, .slider-shprtcode, .toggle, .basic .accord, ul.nav-tab li, .list-wrap, .videos, .avatar img, .alignleft-f {
		    background-color: <?php echo $color; ?>
        }

        /* for ie8 */
        #ie8 #wp-calendar td, #ie7 #calendar_wrap, #ie7 #wp-calendar caption, #ie7 #aside .slider-wrapper, #ie7 .half .slider-wrapper, #ie7 .full .slider-wrapper, #ie7 .two-thirds .slider-wrapper, #ie7 .one-fourth .slider-wrapper, #ie7 #aside .slider_container_1, #ie7 .slider_container_1 {
            filter: <?php echo $ie_color; ?>
		}

		#ie8 .slider-shortcode, #ie7 .contact-block, #ie7 .about, #ie7 .textwidget-photo, #ie7 .partner-bg, #ie7 .alignleft, #ie7 .alignright, #ie7 .aligncenter, #ie7 .gallery-item, #ie7 .shadow-light, #ie7 .reviews-t, #ie7 #aside .twit .reviews-t, #ie7 .blockquote-bg, #ie7 .navigation, #ie7 .map, #ie7 .navig-category, #ie7 .slider-shprtcode, #ie7 .toggle, #ie7 .basic .accord, #ie7 ul.nav-tab li, #ie7 .list-wrap, #ie7 .videos, #ie7 .avatar img, #ie7 .alignleft-f {
		    filter: <?php echo $ie_color; ?>
		}
        
        /* for ie8 */
        #ie8 #wp-calendar td, #ie8 #calendar_wrap, #ie8 #wp-calendar caption, #ie8 #aside .slider-wrapper, #ie8 .half .slider-wrapper, #ie8 .full .slider-wrapper, #ie8 .two-thirds .slider-wrapper, #ie8 .one-fourth .slider-wrapper, #ie8 #aside .slider_container_1, #ie8 .slider_container_1 {
            -ms-filter: "<?php echo $ie_color; ?>";
		}

		#ie8 .slider-shortcode, #ie8 .contact-block, #ie8 .about, #ie8 .textwidget-photo, #ie8 .partner-bg, #ie8 .alignleft, #ie8 .alignright, #ie8 .aligncenter, #ie8 .gallery-item, #ie8 .shadow-light, #ie8 .reviews-t, #ie8 #aside .twit .reviews-t, #ie8 .blockquote-bg, #ie8 .navigation, #ie8 .map, #ie8 .navig-category, #ie8 .slider-shprtcode, #ie8 .toggle, #ie8 .basic .accord, #ie8 ul.nav-tab li, #ie8 .list-wrap, #ie8 .videos, #ie8 .avatar img, #ie8 .alignleft-f {
            -ms-filter: "<?php echo $ie_color; ?>";
		}
        
        .reviews-b {
			border-top: 12px solid <?php echo $color; ?>
        }
/*
		#footer .reviews-b {
			border-right: 12px solid transparent;
			border-top: 12px solid rgba(255,255,255,0.17);
			border-left: 0px solid transparent;
		}
		.blockquote-bot {
			border-right: 12px solid transparent;
			border-top: 12px solid rgba(255,255,255,0.20);
			border-left: 0px solid transparent;   
		}
*/
        /* background headers homepage background */
        #home-bg {
            background-image: <?php echo dt_style_options_get_image(
                array(),
                of_get_option('header_homepage-bg_image', 'none'),
                of_get_option('header_homepage-bg_custom'),
                of_get_option('header_homepage-bg_upload')
            );
            ?>
            background-position: <?php echo dt_style_options_get_bg_position(
                of_get_option('header_homepage-bg_horizontal_pos'),
                of_get_option('header_homepage-bg_vertical_pos')
            ); ?>
            background-repeat: <?php echo of_get_option('header_homepage-bg_repeat'); ?>;
        }

        /* background headers content background */
        #bg {
            background-image: <?php echo dt_style_options_get_image(
                array(),
                of_get_option('header_content-bg_image', 'none'),
                of_get_option('header_content-bg_custom'),
                of_get_option('header_content-bg_upload')
            );
            ?>
            background-position: <?php echo dt_style_options_get_bg_position(
                of_get_option('header_content-bg_horizontal_pos'),
                of_get_option('header_content-bg_vertical_pos')
            ); ?>
            background-repeat: <?php echo of_get_option('header_content-bg_repeat'); ?>;
        }
        
        /* background headers contact background color/opacity */
        .contact-block {
            background-color: <?php echo dt_style_options_get_rgba_from_hex_color(
                array(),
                of_get_option('contact-bg_color'),
                of_get_option('contact-bg_opacity')
            ); ?>
        }
        /* for ie7 */
        #ie7 .contact-block {
            filter: <?php echo dt_style_options_get_rgba_from_hex_color_for_ie(
                array(),
                of_get_option('contact-bg_color'),
                of_get_option('contact-bg_opacity')
            ); ?>
        }
        /* for ie8 */
        #ie8 .contact-block {
            -ms-filter: "<?php echo dt_style_options_get_rgba_from_hex_color_for_ie(
                array(),
                of_get_option('contact-bg_color'),
                of_get_option('contact-bg_opacity')
            ); ?>";
        }

        
        
        /* background headers top line background */
        #top-bg {
            background-image: <?php $image = dt_style_options_get_image(
                array(),
                of_get_option('top_line-bg_image', 'none'),
                of_get_option('top_line-bg_custom'),
                of_get_option('top_line-bg_upload')
            ); echo $image;
            ?>
            background-position: <?php echo dt_style_options_get_bg_position(
                of_get_option('top_line-bg_horizontal_pos'),
                of_get_option('top_line-bg_vertical_pos')
            ); ?>
            background-repeat: <?php echo of_get_option('top_line-bg_repeat'); ?>;
			background-color: <?php echo dt_style_options_get_rgba_from_hex_color(
                array(),
                of_get_option('top_line-bg_color'),
                of_get_option('top_line-bg_opacity')
            ); ?>
        }

        <?php if( 'none' == $image ): ?>
        /* for ie7 */
        #ie7 #top-bg {
            filter: <?php echo dt_style_options_get_rgba_from_hex_color_for_ie(
                array(),
                of_get_option('top_line-bg_color'),
                of_get_option('top_line-bg_opacity')
            ); ?>
        }
        /* for ie8 */
        #ie8 #top-bg {
            -ms-filter: "<?php echo dt_style_options_get_rgba_from_hex_color_for_ie(
                array(),
                of_get_option('top_line-bg_color'),
                of_get_option('top_line-bg_opacity')
            ); ?>";
        }
        <?php endif; ?>

        /* background parallax position */
        #parallax {
            position: <?php echo (of_get_option('mr_parallax-fixed')?'fixed':'absolute'); ?> !important;
        }

        /* background footer baground */
        #footer {
            background-image: <?php echo dt_style_options_get_image(
                array(),
                of_get_option('footer-bg_image', 'none'),
                of_get_option('footer-bg_custom'),
                of_get_option('footer-bg_upload')
            );
            ?>
            background-position: <?php echo dt_style_options_get_bg_position(
                of_get_option('footer-bg_horizontal_pos'),
                of_get_option('footer-bg_vertical_pos')
            ); ?>
            background-repeat: <?php echo of_get_option('footer-bg_repeat'); ?>;
            background-color: <?php echo of_get_option('footer-bg_color'); ?>;
        }
        
        /* background footer bottom line bg */
        #bottom {
            background-image: <?php $image = dt_style_options_get_image(
                array(),
                of_get_option('bottom_line-bg_image', 'none'),
                of_get_option('bottom_line-bg_custom'),
                of_get_option('bottom_line-bg_upload')
            ); echo $image;
            ?>
            background-position: <?php echo dt_style_options_get_bg_position(
                of_get_option('bottom_line-bg_horizontal_pos'),
                of_get_option('bottom_line-bg_vertical_pos')
            ); ?>
            background-repeat: <?php echo of_get_option('bottom_line-bg_repeat'); ?>;
			background-color: <?php echo dt_style_options_get_rgba_from_hex_color(
                array(),
                of_get_option('bottom_line-bg_color'),
                of_get_option('bottom_line-bg_opacity')
            ); ?>
        }
        
        <?php if( 'none' == $image ): ?> 
        /* for ie7 */
        #ie7 #bottom {
            filter: <?php echo dt_style_options_get_rgba_from_hex_color_for_ie(
                array(),
                of_get_option('bottom_line-bg_color'),
                of_get_option('bottom_line-bg_opacity')
                
            ); ?>
        }
        /* for ie8 */
        #ie8 #bottom {
            -ms-filter: "<?php echo dt_style_options_get_rgba_from_hex_color_for_ie(
                array(),
                of_get_option('bottom_line-bg_color'),
                of_get_option('bottom_line-bg_opacity')
                
            ); ?>";
        }
        <?php endif; ?>

        .custom-menu li.current-menu-item a {
            color: #121313;
            text-shadow: #A7B0B1 1px 1px 0;
        }

        /* background footer wide divider */
        .line-footer {
            background-image: <?php echo dt_style_options_get_image(
                array(),
                of_get_option('divs_and_heads-footer_wide_divider', 'none'),
                of_get_option('divs_and_heads-footer_wide_divider_custom'),
                of_get_option('divs_and_heads-footer_wide_divider_upload')
            );
            ?>
            background-position: <?php echo dt_style_options_get_bg_position( 'bottom', 'center' ); ?>
            background-repeat: <?php echo of_get_option('divs_and_heads-footer_wide_divider_repeatx')?'repeat-x':'no-repeat'; ?>;
        }

        /* background footer narrow divider */
        #footer .post, #footer ul.categories li, #footer .custom-menu li {
            background-image: <?php echo dt_style_options_get_image(
                array(),
                of_get_option('divs_and_heads-footer_narrow_divider', 'none'),
                of_get_option('divs_and_heads-footer_narrow_divider_custom'),
                of_get_option('divs_and_heads-footer_narrow_divider_upload')
            );
            ?>
            background-position: <?php echo dt_style_options_get_bg_position( 'top', 'left' ); ?>
            background-repeat: <?php echo of_get_option('divs_and_heads-footer_narrow_divider_repeatx')?'repeat-x':'no-repeat'; ?>;
        }
        
    <?php
    $color = dt_style_options_get_rgba_from_hex_color(
        array(),
        of_get_option('widgetcodes_footer-bg_color'),
        of_get_option('widgetcodes_footer-bg_opacity')
    );
    $ie_color = dt_style_options_get_rgba_from_hex_color_for_ie(
        array(),
        of_get_option('widgetcodes_footer-bg_color'),
        of_get_option('widgetcodes_footer-bg_opacity')
    );
    ?>

        /* background footer widgets/shortcodes bg */
		#footer #wp-calendar td, #footer #calendar_wrap, #footer #wp-calendar caption, #footer .reviews-t, #footer .partner-bg, .twit .reviews-t, #footer .flickr .alignleft-f, #footer .slider_container_1, #footer .textwidget-photo, #footer .alignleft {
		    background-color: <?php echo $color; ?>
        }

        /* for ie7 */
		#ie7 #footer #wp-calendar td, #ie7 #footer #calendar_wrap, #ie7 #footer #wp-calendar caption, #ie7 #footer .reviews-t, #ie7 #footer .partner-bg, #ie7 .twit .reviews-t, #ie7 #footer .flickr .alignleft-f, #ie7 #footer .slider_container_1, #ie7 #footer .textwidget-photo, #ie7 #footer .alignleft {
            filter: <?php echo $ie_color; ?> 
        }

        /* for ie8 */
		#ie8 #footer #wp-calendar td, #ie8 #footer #calendar_wrap, #ie8 #footer #wp-calendar caption, #ie8 #footer .reviews-t, #ie8 #footer .partner-bg, #ie8 .twit .reviews-t, #ie8 #footer .flickr .alignleft-f, #ie8 #footer .slider_container_1, #ie8 #footer .textwidget-photo, #ie8 #footer .alignleft {
            -ms-filter: "<?php echo $ie_color; ?>"; 
        }

        #footer .reviews-b {
			border-top: 12px solid <?php echo $color; ?>
        }
        
    <?php
    $color = of_get_option('fonts_content-topline_color') . ' !important;';
    $shadow_color = dt_get_shadow_color( of_get_option('fonts_content-topline_shadow_color') );
    ?>
       /* top line */ 
        .right-top a, .right-top li { 
            color: <?php echo $color; ?>
            text-shadow: <?php echo $shadow_color; ?> !important;
        }
        
    <?php
    $color = of_get_option('fonts_content-contacts_color') . ' !important;';
    $shadow_color = dt_get_shadow_color( of_get_option('fonts_content-contacts_shadow_color') );
	?>
       /* content block */ 
        .contact-block span{
            color: <?php echo $color; ?>
            text-shadow: <?php echo $shadow_color; ?> !important;
        }
        
    <?php
    $color = of_get_option('fonts_content-secondary_color') . ' !important;';
    $shadow_color = dt_get_shadow_color( of_get_option('fonts_content-secondary_shadow_color') );
    ?>
        /* fonts content secondary color */
        a, span.tooltip, .widget .ico-link.comments, .head-capt, .mid-gray, .panel-wrapper .blue-date, p.autor, p.autor a, .goto-post span, .entry-meta .ico-link, .ico-link a, .autor-head, .head, .comment-meta span, #comments .comment-meta a, #form-holder .do-clear, .price, .full-left a {
            color: <?php echo $color; ?>
            text-shadow: <?php echo $shadow_color; ?> !important;
        }
        .comment-meta {
            color: <?php echo $color; ?>
        }
        span.tooltip {
            border-bottom: 1px dashed <?php echo $color; ?>;
        }
        
    <?php $color = of_get_option('fonts-links_color') . ' !important;'; ?>

        /* fonts main menu links color */
        .ico-link.comments, ul#nav li a {
            color: <?php echo $color; ?> 
		}
		
    <?php $color = of_get_option('fonts-hover_color') . ' !important;'; ?>

        /* fonts main menu hover color */
		.paginator li.act a, .widget-info .head, .widget-info .head-capt, .widget-info .hide-me, .widget-info .details,  #nav li div ul li a.act, ul#nav > li > a.act, #nav > li div ul li:hover > a, .button.act {
			color: <?php echo $color; ?>
		}
		.widget-info .head, .widget-info .head-capt, .widget-info .details, .widget-info .hide-me {
			text-shadow: none !important;
		}
    
    <?php
	$color = of_get_option('fonts_footer-primary_color') . ' !important;';
	$shadow_color = dt_get_shadow_color( of_get_option('fonts_footer-primary_shadow_color') );
	?>

        /* footer primary colors */
        #footer .text-photo, #footer .head, #footer .custom-menu li a, #footer .panel .panel-wrapper a, #footer .reviews-t, #footer .categories a, #footer .post a, #footer a.do-clear, #footer .dt_captcha, .foot-cont {			
            color: <?php echo $color; ?>
            text-shadow: <?php echo $shadow_color; ?> !important;
		}

    <?php
    $color = of_get_option('fonts_footer-secondary_color') . ' !important;';
    $shadow_color = dt_get_shadow_color( of_get_option('fonts_footer-secondary_shadow_color') );
    ?>

        #footer .mid-gray, #footer .panel-wrapper .blue-date, #footer p.autor, #footer p.autor a, #footer .goto-post span, #footer a, #footer .goto-post span, .foot-cont p.autor, .foot-cont p.autor a {
			color: <?php echo $color; ?>
			text-shadow: <?php echo $shadow_color; ?> !important;
		}

    <?php
    $color = of_get_option('fonts_content-bottomline_color') . ' !important;';
    $shadow_color = dt_get_shadow_color( of_get_option('fonts_content-bottomline_shadow_color') );
    ?>
       /* bottom line */ 
        #footer .bottom-cont span, #footer .bottom-cont a { 
            color: <?php echo $color; ?>
            text-shadow: <?php echo $shadow_color; ?> !important;
        }
        
<?php 
    $headers = array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6' );
    foreach( $headers as $header ):
        $font_size = of_get_option('fonts-headers_size_' . $header);
?>
        /* font size */
        <?php echo $header; ?>, #container > <?php echo $header; ?>{
            font-size: <?php echo $font_size; ?>px !important;
	    }
        
        <?php if( 'h3' == $header ): ?>
            #carousel .caption-head,
			#ps-slider .ps-head h3,
			.gallery .textwidget.text .head,
			.gallery .textwidget.text .hide-me,
			.header,
			.dt-search-page .textwidget.text .head {
                font-size: <?php echo $font_size; ?>px !important;
            }
        <?php endif; ?>

        <?php if( 'h2' == $header ): ?>
            #fancytile-slide .caption-head, #slider .caption-head, #form-holder .header { 
                font-size: <?php echo $font_size; ?>px !important;
            }
        <?php endif; ?>
            
    <?php endforeach; ?>
        
    <?php
/*
    $aside_opts = get_post_meta( $post->ID, '', true );
    $align = 'right';
    if( !empty($aside_opts) && 'left' == $aside_opts['align'] ) {
        $align = 'left';
    }
 */  ?>
    
    #slide .text-capt, #slide .ps-cont {
        color: #2b2b2b !important;
    }
    
        <?php
        $content_color_1 = of_get_option('fonts_content-headers_top_color');
        $content_color_2 = of_get_option('fonts_content-headers_bottom_color');
        $content_shadow_color = dt_get_shadow_color( of_get_option('fonts_content-headers_shadow_color') );

        $footer_color_1 = of_get_option('fonts_footer-headers_top_color');
        $footer_color_2 = of_get_option('fonts_footer-headers_bottom_color');
        $footer_shadow_color = dt_get_shadow_color( of_get_option('fonts_footer-headers_shadow_color') );
        ?> 

    .textwidget .info .head, h1, h1 a, h2, h2 a, h3, h3 a, h4, h4 a, h5, h5 a, h6, h6 a, .double-header .first-head, .header, #form-holder .header, .ps-head h3 {
        color: <?php echo $content_color_2; ?> !important;
        text-shadow: <?php echo $content_shadow_color; ?> !important;
    }
    
    .foot-cont .header {
        color: <?php echo $footer_color_2; ?> !important;
        text-shadow: <?php echo $footer_shadow_color; ?> !important;
    }

    /* accordion, toggle, tabs */ 
    .accord a, .nav-tab a, .toggle .question {
        color: <?php echo of_get_option('fonts_content-secondary_color'); ?> !important;
        text-shadow: <?php echo of_get_option('fonts_content-secondary_shadow_color'); ?> !important;
    }
        
    .accord a.selected, .nav-tab a.current, .toggle .question.act {
        color: <?php echo of_get_option('fonts_content-primary_color'); ?> !important; 
        text-shadow: <?php echo of_get_option('fonts_content-primary_shadow_color'); ?> !important;
    }

    </style>
	<?php
	$content_shadow_color = of_get_option('fonts_content-headers_shadow_color');
	if( $content_shadow_color )
		$content_shadow_color = '1px 1px '. $content_shadow_color;
	else
		$content_shadow_color = 'none';
	
	$footer_shadow_color = of_get_option('fonts_footer-headers_shadow_color');
	if( $footer_shadow_color )
		$footer_shadow_color = '1px 1px '. $footer_shadow_color;
	else
		$footer_shadow_color = 'none';
	?>
    <script>

        function cufon_in_gall() {
            Cufon('.textwidget > .info > .head', {
	            color: '-linear-gradient(<?php echo $content_color_1; ?>, <?php echo $content_color_2; ?>)', textShadow: '<?php echo $content_shadow_color; ?>'
            });
        }
 
        Cufon('h1, h2, h3, h4, h5, h6', {
            color: '-linear-gradient(<?php echo $content_color_1; ?>, <?php echo $content_color_2; ?>)', textShadow: '<?php echo $content_shadow_color; ?>'
        });

        Cufon('.double-header .first-head', {
            color: '-linear-gradient(<?php echo $content_color_1; ?>, <?php echo $content_color_2; ?>)', textShadow: '<?php echo $content_shadow_color; ?>'
        });

        Cufon('.header', {
            color: '-linear-gradient(<?php echo $content_color_1; ?>, <?php echo $content_color_2; ?>)', textShadow: '<?php echo $content_shadow_color; ?>'
        });

        Cufon('#form-holder .header', {
            color: '-linear-gradient(<?php echo $content_color_1; ?>, <?php echo $content_color_2; ?>)', textShadow: '<?php echo $content_shadow_color; ?>'
        });

        Cufon('.ps-head h3', {
            color: '-linear-gradient(#f2f2f2, #f2f2f2)'
        });

        Cufon('.foot-cont .header', {
            color: '-linear-gradient(<?php echo $footer_color_1; ?>, <?php echo $footer_color_2; ?>)', textShadow: '<?php echo $footer_shadow_color; ?>'
        });
        
    </script>
    <?php
}

function dt_add_post_formats_support() {
    add_theme_support( 'post-formats', array( 'video', 'gallery', 'quote', 'status' ) );
}
//add_action( 'after_setup_theme', 'dt_add_post_formats_support' );

function dt_redirect_for_standard_post_format() {
    if( 'post-format-standard' == get_query_var('post_format') ) {
        locate_template( array( 'archive.php', 'index.php' ), true );
        exit;
    }
}
add_action( 'template_redirect', 'dt_redirect_for_standard_post_format' );

// gallery shortcode filter
function dt_filter_gallery_sc($output, $attr) {
	global $post, $wp_locale;
	$exclude_def = '';
    $event = '';
	if( $hide_in_gal = get_post_meta( $post->ID, 'hide_in_gal', true ) && ('gallery' == get_post_format($post->ID)) ) {
		$exclude_def = get_post_thumbnail_id( $post->ID );
	}
	// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] )
			unset( $attr['orderby'] );
	}
		
	extract(shortcode_atts(array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post->ID,
		'itemtag'    => 'li',
		'columns'    => 3,
		'size'       => 'thumbnail',
		'include'    => '',
		'exclude'    => $exclude_def
	), $attr));

	$id = intval($id);
	if ( 'RAND' == $order )
		$orderby = 'none';

	if ( !empty($include) ) {
		$include = preg_replace( '/[^0-9,]+/', '', $include );
		$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( !empty($exclude) ) {
		$exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
		$attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	} else {
		$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	}

	if ( empty($attachments) )
		return '';

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment )
			$output .= wp_get_attachment_link($att_id, $size, true) . "\n";
		return $output;
	}

	if( isset($attr['link']) && ('file' == $attr['link']) ) {
        $event .= 'onclick="return hs.expand(this, { slideshowGroup: \'' . $post->ID . '\' })"';
		$hs_class = " hs_me";
	}else {
		$hs_class = " to_attachment";
	}
	
	$itemtag = tag_escape($itemtag);
	$columns = intval($columns);
	$size_class = sanitize_html_class( $size );
	
	$output = "<ul class='gall_std{$hs_class} gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";

	$i = 0;
	foreach ( $attachments as $id => $attachment ) {
		$class = $description = '';
        $class .= 'highslide ';
		if( isset($attr['link']) && ('file' == $attr['link']) ) {
			$href = wp_get_attachment_image_src($id, 'full');
			$href = $href?current($href):'#';
			$class .= "fadeThis ";
		}else {
			$href = get_permalink($id);
		}
		
		if( $attachment->post_content ) {
			$description = '<p class="wp-caption-text">'.wptexturize($attachment->post_content).'</p>';
			$class .= 'wp-caption ';
		}
		
		$caption = wptexturize(trim($attachment->post_excerpt));
		$dt_img = dt_get_thumbnail(
            array(
                'img_id'	=> $id,
                'width'		=> 115,
                'height'    => 115,
                'upscale'	=> true,
                'quality'   => 90,
                'zc_align'  => 'c'
            )        
        );
        $src[0] = $dt_img['thumnail_img'];
        $src[1] = $dt_img['width'];
        $src[2] = $dt_img['height'];
        //$src = wp_get_attachment_image_src($id, $size);
        
		$link = "<a class='{$class}' href='{$href}' title='{$caption}' {$event}>
		<img src='{$src[0]}' alt='' width='{$src[1]}' height='{$src[2]}'/>{$description}
		</a>";
        
        $li_size = $src[1];
        
		$output .= "<{$itemtag} class='shadow_light gallery-item' style='width: {$li_size}px;'>";
		$output .= $link;
		$output .= "</{$itemtag}>";
	}

	$output .= "</ul>\n";

	return $output;
}
add_filter('post_gallery', 'dt_filter_gallery_sc', 10, 2);

add_filter('body_class','dt_body_class_names');
function dt_body_class_names($classes) {
    foreach( $classes as $index=>$class ) {
        if( 'search' == $class ) {
            unset($classes[$index]);
        }
    }
	return $classes;
}

add_filter( 'the_password_form', 'dt_password_form' );
function dt_password_form() {
    global $post, $paged;
    $http_referer = wp_referer_field( false );
    $page_data = dt_storage( 'page_data' ); 
    $wp_ver = explode('.', get_bloginfo('version'));
    $wp_ver = array_map( 'intval', $wp_ver );
    $form_action = esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ); 

    if( $page_data && isset($page_data['base_url']) && isset($page_data['cat_id']) && isset($page_data['layout']) ) {
        $site_url = site_url();
        $http_referer = str_replace( str_replace($site_url, '', admin_url('admin-ajax.php')), str_replace($site_url, '', $page_data['base_url']).'#'.current($page_data['cat_id']).'/'.$paged.'/'.$page_data['layout'], $http_referer );
    }
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<div class="form-protect"><form class="protected-post-form get-in-touch" action="'. $form_action. '" method="post">'. $http_referer. '
    <div>' . __( "To view this protected post, enter the password below:", LANGUAGE_ZONE ) . '</div>
    <label for="' . $label . '">' . __( "Password:", LANGUAGE_ZONE ) . '&nbsp;&nbsp;&nbsp;</label><div class="i-h"><input name="post_password" id="' . $label . '" type="password" size="20" /></div>
	<a title="Submit" class="button go_submit" onClick="submit();" href="#"><span>' . esc_attr__( "Submit" ) . '</span></a>
	
	
    </form></div>
    ';
    return $o;
}


function dt_exclude_post_protected_filter( $where ) {
    return $where.' AND post_password=""';
}

if( false ) {
    ob_start();
    post_class();
    comment_form();
    ob_get_clean();
}

/* Add apropriet class to portfolio/gallery/category in search
 * Used in filter "dt_portfolio_classes" (functions.php), search.php
 */
function dt_search_portfolio_class_filter ( $class, $type = '', $place = '' ) {
	if ( isset( $class['2_col']['list']['sidebar']['block'] ) && 'block' == $place ) {
		$new_class = ' item-blog';
		
		if ( 1 === dt_storage( 'post_is_first' ) ) {
			$new_class .= ' first';
			dt_storage( 'post_is_first', -1 );
		}
		$class['2_col']['list']['sidebar']['block'] .= $new_class;
	}
	return $class;
}
