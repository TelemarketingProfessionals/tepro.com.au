<?php
/* Template Name: 09. Photos with Sidebar */
?>
<?php get_header(); ?>
<?php dt_storage('have_sidebar', true); ?>

<?php get_template_part('top-bg'); ?>

<?php get_template_part('parallax'); ?>

<div id="wrapper">

    <?php get_template_part('nav'); ?>
    
    <?php get_sidebar( 'left' ); ?>

    <div id="container">

        <h1><?php the_title(); ?></h1>
        <div class="hr hr-wide gap-big"></div>

        <div class="gallery">

            <?php if( !post_password_required() ): ?>
           
            <?php
            global $post;
            $opts = get_post_meta($post->ID, '_dt_photos_layout_options', true);
            $cats = get_post_meta($post->ID, '_dt_photos_layout_albums', true);
            
            $args = array(
                'post_type'         => 'dt_gallery',
                'taxonomy'          => 'dt_gallery_category',
                'select'            => $cats['select'],
                'layout'            => $opts['layout'],
                'count_attachments' => true,
                'show'              => ('on' == $opts['show_cat_filter'])?true:false,
                'layout_switcher'   => false
            );

            $args['terms'] = array();
            if( 'albums' == $cats['type'] && isset($cats['albums']) && ('all' != $cats['select']) ) {
                global $wpdb;
                    
                    $terms_str = implode(',', array_values($cats['albums']));

                    $terms = $wpdb->get_results( "
                        SELECT $wpdb->term_taxonomy.term_id AS ID    
                        FROM $wpdb->posts
                        JOIN $wpdb->term_relationships ON $wpdb->term_relationships.object_id = $wpdb->posts.ID
                        JOIN $wpdb->term_taxonomy ON $wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id
                        WHERE $wpdb->posts.post_type = 'dt_gallery'
                        AND $wpdb->posts.post_status = 'publish'
                        AND $wpdb->posts.ID IN ($terms_str)
                        GROUP BY $wpdb->term_taxonomy.term_id
                    " );
                if( $terms ) {
                    foreach( $terms as $term ) {
                        $args['terms'][] = intval($term->ID);    
                    }
                }
                $args['post_ids'] = $cats['albums'];
            }elseif( isset($cats['albums_cats']) && ('all' != $cats['select']) ) {
                $args['terms'] = $cats['albums_cats'];
            }

            dt_category_list( $args );
            ?>

            <div class="gallery-inner t-l dt-ajax-content"></div>

            <?php else: ?>
            
            <?php echo get_the_password_form(); ?>

            <?php endif; ?>

        </div>

    </div>
        
        <?php get_sidebar( 'right' ); ?>

</div>

<?php get_footer(); ?>
