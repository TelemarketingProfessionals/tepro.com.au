<?php
global $post;
if( isset($post) && !empty($post) ) {
    $opts = get_post_meta($post->ID, '_dt_layout_sidebar_options', true);
}else {
    $opts = array();
}

if( (isset($opts['align']) && ('right' == $opts['align'])) || !isset($opts['align']) ):
?>

<!-- right sidebar -->
<aside id="aside" class="right">
<?php dt_aside_widgetarea(); ?>
</aside>

<?php endif; ?>
