<?php
/* Template Name: 04. Page with Sidebar */
?>
<?php get_header(); ?>
<?php dt_storage('have_sidebar', true); ?>

<?php get_template_part('top-bg'); ?>

<?php get_template_part('parallax'); ?>

<div id="wrapper">

<?php get_template_part('nav'); ?>

<?php get_sidebar( 'left' ); ?>

<div id="container">

<?php
if( have_posts() ) {
    while( have_posts() ) { the_post(); the_content(); }
}
?>

</div>

<?php get_sidebar( 'right' ); ?>

</div>

<?php get_footer(); ?>
