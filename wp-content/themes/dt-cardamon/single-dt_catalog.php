<?php get_header(); ?>
<?php dt_storage('have_sidebar', true); ?>
    
    <?php get_template_part('top-bg'); ?>
    
    <?php get_template_part('parallax'); ?>
    
    <div id="wrapper">
        
        <?php get_template_part('nav'); ?>
        
        <div id="container">
            
            <?php if( have_posts() ): while( have_posts() ): the_post(); ?>
            
                <h1><?php the_title(); ?></h1>
                <div class="hr hr-wide gap-big"></div>

            <?php
            global $post;
            $post_opts = get_post_meta($post->ID, '_dt_catalog-post_options', true);

            if( !isset($post_opts['hide_media']) || (isset($post_opts['hide_media']) && !$post_opts['hide_media']) ) {
                $args = array(
                    'post_type'         => 'attachment',
                    'post_status'       => 'inherit',
                    'posts_per_page'    => -1,
                    'post_parent'       => $post->ID,
                    'post_mime_type'    => 'image',
                    'orderby'           => 'menu_order',
                    'order'             => 'ASC'
                );
                if( $post_opts['hide_thumbnail'] )
                    $args['post__not_in'] = array( get_post_thumbnail_id() );

                $dt_tmp_query = new WP_Query( $args );
                if( $dt_tmp_query->have_posts() ) {
                    $slides = array();
                    foreach( $dt_tmp_query->posts as $slide ) {
                        $video = get_post_meta( $slide->ID, '_dt_catalog_video_link', true );
                        $tmp_arr = array();

                        $tmp_arr['caption'] = $slide->post_excerpt;
                        if( !$video ) {
                            $slide_src = dt_get_resized_img( wp_get_attachment_image_src($slide->ID, 'full'), array('w' => 702) );
                            $tmp_arr['src'] = $slide_src[0];
                            $tmp_arr['size_str'] = $slide_src[3];
                        }else {
                            $tmp_arr['is_video'] = true; 
                            $tmp_arr['src'] = $video; 
                            $tmp_arr['size_str'] = array( 702, 1024 );
                        }
                        $slides[] = $tmp_arr;
                    }
                    dt_get_anything_slider( array('id' => 'slider2', 'items_arr' => $slides) );
                }
            }
            ?>
                
                <?php $opts = get_post_meta($post->ID, '_dt_catalog-goods_options', true); ?>

                <span class="price"><?php _e('Price: ', LANGUAGE_ZONE); !empty($opts['price'])?esc_attr_e($opts['price']):_e('Undefined', LANGUAGE_ZONE); ?></span>
                
                <?php the_content(); ?>

                <?php if( !empty($opts['p_link']) ): ?>
                
                    <a href="<?php echo esc_url($opts['p_link']); ?>" class="button" title=""><span><i class="dol"></i><?php _e('Make purchase!', LANGUAGE_ZONE); ?></span></a>
                
                <?php endif; ?>

                <p class="gap"></p>
                <?php
                $rel_works = get_post_meta($post->ID, '_dt_catalog_related', true);
                if( isset($rel_works['show_related']) && $rel_works['show_related'] ):
                    if( 'same' == $rel_works['related'] ) {
                        $rel_works['related'] = wp_get_post_terms(
                            $post->ID,
                            'dt_catalog_category',
                            array('fields' => 'ids')
                        );
                    }
                    if( !empty($rel_works['related']) ):
                ?>

                <p class="hr hr-narrow gap-small"></p>
                
                <div class="gap"></div>
                <div class="full-width w-photo">
                    <h2><?php _e('Related Items', LANGUAGE_ZONE); ?></h2>
            
                    <?php
                    if( 'same' == $rel_works['related'] ) {
                        $rel_works['related'] = wp_get_post_terms(
                            $post->ID,
                            'dt_catalog_category',
                            array('fields' => 'ids')
                        );
                    }
                    $dt_tmp_query = new WP_Query( array(
                        'posts_per_page'    => -1,
                        'post_type'         => 'dt_catalog',
                        'post_status'       => 'publish',
                        'post__not_in'      => array($post->ID),
                        'tax_query'         => array( array(
                            'taxonomy'  => 'dt_catalog_category',
                            'field'     => 'id',
                            'terms'     =>  $rel_works['related'],
                            'operator'  => 'IN'
                        ) )    
                    ) );
                    if( $dt_tmp_query->have_posts() ) {
                        $thumb_arr = dt_core_get_posts_thumbnails( $dt_tmp_query->posts );
                        $items = array();
                        foreach( $dt_tmp_query->posts as $rel_post ) {
                            $item = array();
                            $img = dt_get_resized_img(
                                dt_get_thumb_meta($thumb_arr['thumbs_meta'], 'full', $rel_post->ID),
                                array('w' => 216, 'h' => 122, 'use_noimage' => true)
                            );
                            $item['src'] = $img[0];
                            $item['size_str'] = $img[3];
                            $item['post_id'] = $rel_post->ID;
                            $items[] = $item;
                        }
                        dt_get_carousel_slider( array('items_arr' => $items, 'id' => '', 'class' => 'list-carousel recent') );
                    }
                    ?>
    
                </div>

                <?php endif; endif; ?>
                
                <?php comments_template(); ?>

            <?php
                endwhile;
            endif;
            ?>
        
        </div>

        <aside id="aside" class="right">
        <?php dt_widget_area('', null, 'sidebar_4'); ?>
        </aside>
    
    </div>

<?php get_footer(); ?>
