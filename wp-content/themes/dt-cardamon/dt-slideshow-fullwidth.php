<?php
/* Template Name: 01. Page with Slideshow (Full-width) */
dt_storage('is_homepage', 'true');
?>
<?php get_header(); ?>

    <?php get_template_part('top-bg'); ?>

    <?php get_template_part('parallax'); ?>

    <?php get_template_part('nav'); ?>

    <?php get_template_part('slider'); ?>

        <div id="container" class="full-width"><?php the_content(); ?></div>

<?php get_footer(); ?>
