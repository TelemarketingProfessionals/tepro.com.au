<?php
/*
Template Name: Featured Page
*/
?>

<?php get_header(); ?>

<div id="content">

<div id="featuredtop">
	
	<img src="<?php bloginfo('template_url'); ?>/images/fp-main.jpg" alt="<?php bloginfo('name'); ?>" />
	
	<h1>Using a Featured Page</h1>
	<p>If you would like to enable a featured page, it's quite easy. As you create a page in WordPress, make sure you select Featured Page for the page template.</p>
	<p>This is an area on your website where you can add text. This will serve as an informative location on your website, where you can talk about your site.</p>
	<p>If you are looking to bring back site visitors, make this an informative paragraph that links to a more detailed explanation of what your site is all about.</p>
				
</div>

	<div id="contentleft">
	
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<h1><?php the_title(); ?></h1>
	
		<?php the_content(__('Read more'));?>

		<?php endwhile; else: ?>
		
		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p><?php endif; ?>
	
	</div>
	
<?php include(TEMPLATEPATH."/sidebar_right_page.php");?>
	
</div>

<!-- The main column ends  -->

<?php get_footer(); ?>