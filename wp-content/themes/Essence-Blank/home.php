<?php get_header(); ?>

<div id="hometop">
	<div id="rotator-wrapper" style="position:relative;">
		<?php do_action( 'ithemes_featured_images_fade_images' ); ?>
	</div>
	
	<h1><?php echo $wp_theme_options['homepage_title'] ?></h1>
	<?php echo stripslashes(wpautop($wp_theme_options['homepage_copy'])); ?>
</div>

<?php get_footer(); ?>
