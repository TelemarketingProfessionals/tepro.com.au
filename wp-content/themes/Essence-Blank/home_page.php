<?php
/*
Template Name: Home Page Template
*/
?>
<?php get_header(); ?>

<div id="hometop">
	
	<img src="<?php bloginfo('template_url'); ?>/images/hp-main.jpg" alt="<?php bloginfo('name'); ?>" />
    
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<h1><?php the_title(); ?></h1>
	
		<?php the_content(__('Read more'));?>
		
		<?php endwhile; else: ?>
		
		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p><?php endif; ?>
    
</div>

<?php get_footer(); ?>