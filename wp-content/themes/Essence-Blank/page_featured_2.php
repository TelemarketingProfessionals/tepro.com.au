<?php
/*
Template Name: Featured Page 2
*/
?>

<?php get_header(); ?>

<div id="newspage">

	<div id="newspageleft">
	
		<h2>Featured Category #1</h2>
		<img src="<?php bloginfo('template_url'); ?>/images/np-1.jpg" alt="<?php bloginfo('name'); ?>" />

		<?php $recent = new WP_Query("cat=1&showposts=1"); while($recent->have_posts()) : $recent->the_post();?>
		<b><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></b>
		<?php the_excerpt(__('Read more'));?><div style="clear:both;"></div>
		<?php endwhile; ?><br />
		
		<b>Recent Featured Category #1 Articles</b>
		<ul>
			<?php $recent = new WP_Query("cat=1&showposts=5&offset=1"); while($recent->have_posts()) : $recent->the_post();?>
			<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
			<?php endwhile; ?>
		</ul>
		
	</div>
	
	<div id="newspageright">
	
		<h2>Featured Category #2</h2>
		<img src="<?php bloginfo('template_url'); ?>/images/np-2.jpg" alt="<?php bloginfo('name'); ?>" />

		<?php $recent = new WP_Query("cat=3&showposts=1"); while($recent->have_posts()) : $recent->the_post();?>
		<b><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></b>
		<?php the_excerpt(__('Read more'));?><div style="clear:both;"></div>
		<?php endwhile; ?><br />
		
		<b>Recent Featured Category #2 Articles</b>
		<ul>
			<?php $recent = new WP_Query("cat=3&showposts=5&offset=1"); while($recent->have_posts()) : $recent->the_post();?>
			<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
			<?php endwhile; ?>
		</ul>
			
	</div>
		
</div>

<!-- The main column ends  -->

<?php get_footer(); ?>