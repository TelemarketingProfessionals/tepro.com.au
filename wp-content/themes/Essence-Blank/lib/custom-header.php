<?php
//Custom Header Image Code -- from the WordPress.com API
define('HEADER_TEXTCOLOR', '#555555');
define('HEADER_IMAGE', '%s/images/header.png'); // %s is theme dir uri
define('HEADER_IMAGE_WIDTH', 852);
define('HEADER_IMAGE_HEIGHT', 110);

function essence_header_style() {
?>
<style type="text/css">
#header {
	width: <?php echo HEADER_IMAGE_WIDTH; ?>px;
	height: <?php echo HEADER_IMAGE_HEIGHT; ?>px;
	background: url(<?php header_image(); ?>) top left no-repeat;
}
<?php if ( 'blank' != get_theme_mod('header_textcolor', HEADER_TEXTCOLOR) ) : ?>
#header #headerleft a,
#header #headerleft a:hover,
#header #headerleft p {
	text-indent: 0px;
	color: #<?php header_textcolor() ?>;
}
<?php else : ?>
#header #headerleft,
#header #headerleft a,
#header #headerleft p {
	display: block;
	text-indent: -9999px;
	width: <?php echo HEADER_IMAGE_WIDTH; ?>px;
	height: <?php echo HEADER_IMAGE_HEIGHT; ?>px;
	margin: 0; padding: 0;
	overflow: hidden;
}
<?php endif; ?>
</style>
<?php }
function essence_admin_header_style() {
?>
<style type="text/css">
#headimg {
	margin: 0px; padding: 0px;
	width: <?php echo HEADER_IMAGE_WIDTH; ?>px;
	height: <?php echo HEADER_IMAGE_HEIGHT; ?>px;
	background: url(<?php header_image(); ?>) bottom left no-repeat;
	font-family: Arial, Helvetica, Sans-Serif;
}
#headimg h1 {
	color: #fff;
	font-size: 36px;
	font-family: Times New Roman, Georgia, Arial;
	font-weight: normal;
	margin: 0px;
	padding: 25px 0px 0px 20px;
	text-decoration: none;
}
#headimg h1 a {
	color: #222222;
	font-size: 36px;
	font-family: Times New Roman, Georgia, Arial;
	font-weight: normal;
	margin: 0px;
	padding: 0px 0px 0px 0px;
	text-decoration: none;
}
#headimg #desc {
	color: #222222;
	font-size: 16px;
	font-family: Arial, Tahoma, Verdana;
	font-weight: normal;
	margin: 0px;
	padding: 0px 0px 0px 25px;
	text-decoration: none;
}
</style>
<?php }
add_custom_image_header('essence_header_style', 'essence_admin_header_style');  //Add the custom header
?>
