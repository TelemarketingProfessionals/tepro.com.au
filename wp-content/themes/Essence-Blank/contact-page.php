<?php
/*
Template Name: Contact Page
*/
?>

<?php get_header(); ?>

<div id="content">
	<div id="contentleft">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<h1><?php the_title(); ?></h1>
				
				<?php the_content(); ?>
				
				<?php if ( ! empty( $GLOBALS['ithemes_contact_page'] ) ) : ?>
					<?php $GLOBALS['ithemes_contact_page']->render(); ?>
				<?php endif; ?>
			<?php endwhile; ?>
		<?php else: ?>
			<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		<?php endif; ?>
	</div>
	
	<?php include(TEMPLATEPATH."/sidebar_right_page.php");?>
</div>

<!-- The main column ends -->

<?php get_footer(); ?>
