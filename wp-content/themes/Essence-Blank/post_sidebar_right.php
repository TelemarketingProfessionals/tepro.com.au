<?php
/*
Template Name: Sidebar Right Post
*/
?>

<?php get_header(); ?>

<div id="content">

	<div id="contentleft">
	
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<h1><?php the_title(); ?></h1>
		<p><?php the_time('F j, Y'); ?></p>
	
		<?php the_content(__('Read more'));?>
		
		<div class="postmeta">
			<p>Posted by <?php the_author(); ?> | Filed Under <?php the_category(', ') ?>&nbsp;<?php edit_post_link('(Edit)', '', ''); ?></p>
		</div>
	 			
		<!--
		<?php trackback_rdf(); ?>
		-->
		
		<?php endwhile; else: ?>
		
		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p><?php endif; ?>
		
		<h4>Comments</h4>
		<?php comments_template(); // Get wp-comments.php template ?>
	
	</div>
	
<?php include(TEMPLATEPATH."/sidebar_right_post.php");?>
				
</div>

<!-- The main column ends  -->

<?php get_footer(); ?>