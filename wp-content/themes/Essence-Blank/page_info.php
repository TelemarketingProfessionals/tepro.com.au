<?php
/*
Template Name: Info Page
*/
?>

<?php get_header(); ?>


<div id="infotop">
	
	<img src="<?php bloginfo('template_url'); ?>/images/ip-main.jpg" alt="<?php bloginfo('name'); ?>" />
	
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<h1><?php the_title(); ?></h1>
	
		<?php the_content(__('Read more'));?><div style="clear:both;"></div>

		<?php endwhile; else: ?>
			<h1>Using an Info Page</h1>
	<p>If you would like to enable an info page, it's quite easy. As you create a page in WordPress, make sure you select Info Page for the page template.</p>
	<p>This is an area on your website where you can add text. This will serve as an informative location on your website, where you can talk about your site.</p>
	<p>If you are looking to bring back site visitors, make this an informative paragraph that links to a more detailed explanation of what your site is all about.</p>
		<?php endif; ?>
				
	
</div>

<!-- The main column ends  -->

<?php get_footer(); ?>