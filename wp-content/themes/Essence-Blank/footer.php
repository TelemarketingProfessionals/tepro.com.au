<!-- begin footer -->

<div id="bottom" class="clearfix">

	<div class="bottomleft">
	
		
</div>

<div style="clear:both;"></div>

</div>

<div id="footer" class="clearfix">

	<div class="footerleft">
		<p>Copyright &copy; 2014 <a href="<?php echo get_settings('home'); ?>/"><?php bloginfo('name'); ?></a></p>
		<p>All rights reserved.</p>
	</div>
	
	<div class="footerright">
		<p><a href="http://essence.ithemes.com" title="Dark WordPress Themes" >Blank Essence</a> by <a href="http://ithemes.com" title="Premium WordPress Themes" >iThemes</a></p>
		<p>Powered by <a href="http://wordpress.org" >WordPress</a> &middot; <a href="http://validator.w3.org/check?uri=referer">XHTML</a> &middot; <?php wp_loginout(); ?></p>
	</div>
	
		
</div>

<?php do_action('wp_footer'); ?>

</body>
</html>