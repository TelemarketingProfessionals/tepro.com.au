<!-- begin l_sidebar -->

<div id="l_sidebar">

	<ul id="l_sidebarwidgeted">
	
	<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Left Page Sidebar') ) : else : ?>

		
			
		<li>
		<h2>Text Goes Here</h2>
			<ul>
			<li>This is an area on your website where you can add text using widgets. Each of these text areas can say something different, and is very easy to change.</li>
			</ul>
		</li>
			
		<li>
		<h2>Text Goes Here</h2>
			<ul>
			<li>This is an area on your website where you can add text using widgets. Each of these text areas can say something different, and is very easy to change.</li>
			</ul>
		</li>
		
	<?php endif; ?>
	
	</ul>
	
</div>

<!-- end l_sidebar -->