<?php
/*
Template Name: Blog Index Left Sidebar
*/
?>
<?php get_header(); ?>

<div id="content">

	<div id="contentright">

		<h2>// BLOG</h2>


<?php
$temp = $wp_query;
$wp_query= null;
$wp_query = new WP_Query();
$wp_query->query('showposts=5'.'&paged='.$paged);
if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
global $more; $more = 0;
?>
	
		<h1><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<p><?php the_time('F j, Y'); ?></p>

<?php the_content(); ?>
	

		
<?php endwhile; ?>
<?php else: ?>
		
		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>

<?php endif; ?>
<p><?php posts_nav_link(' &#8212; ', __('&laquo; Previous Page'), __('Next Page &raquo;')); ?></p> 
<?php $wp_query = null; $wp_query = $temp;?>

	</div>
	
<?php include(TEMPLATEPATH."/sidebar_left_post.php");?>
			
</div>

<!-- The main column ends  -->

<?php get_footer(); ?>