<?php
if ( function_exists('register_sidebar') )
register_sidebar(array('name'=>'Left Page Sidebar'));
register_sidebar(array('name'=>'Right Page Sidebar'));
register_sidebar(array('name'=>'Left Post Sidebar'));
register_sidebar(array('name'=>'Right Post Sidebar'));
register_sidebar(array('name'=>'Contact Page Sidebar'));
register_sidebar(array('name'=>'Bottom Left Sidebar','before_widget' => '','after_widget' => '','before_title' => '<h2>','after_title' => '</h2>',));
register_sidebar(array('name'=>'Bottom Middle Sidebar','before_widget' => '','after_widget' => '','before_title' => '<h2>','after_title' => '</h2>',));
register_sidebar(array('name'=>'Bottom Right Sidebar','before_widget' => '','after_widget' => '','before_title' => '<h2>','after_title' => '</h2>',));

//Turns /n to <br /> and /n/n to </p><p>
function nls2p($str) {
	$str = str_replace('<p></p>', '', '<p>'
        . preg_replace('#([\r\n]\s*?[\r\n]){2,}#', '</p><p>', $str)
        . '</p>');
	return $str;
}

//A function to include files throughout the theme
//It checks to see if the file exists first, so as to avoid error messages.
function get_template_file($filename) {
	if (file_exists(TEMPLATEPATH."/$filename"))
		include(TEMPLATEPATH."/$filename");
	}

//Theme Options Code
include(TEMPLATEPATH."/lib/theme-options/theme-options.php");

//Custom Header Image code
include(TEMPLATEPATH."/lib/custom-header.php");

//Contact Page Template code
include(TEMPLATEPATH."/lib/contact-page-plugin/contact-page-plugin.php");

//Add Custom Header to theme menu
add_action( 'admin_menu', 'it_custom_header_add_menu', 20 );
function it_custom_header_add_menu() {
    add_submenu_page( $GLOBALS['wp_theme_page_name'], __('Custom Header'), __('Custom Header'), 'edit_themes', 'custom-header', array( &$GLOBALS['custom_image_header'], 'admin_page' ) );
}

add_action( 'ithemes_load_plugins', 'ithemes_functions_after_init' );
function ithemes_functions_after_init() {
	//Include Tutorials Page
	include(TEMPLATEPATH."/lib/tutorials/tutorials.php");
	
	//Featured Image code
	include(TEMPLATEPATH."/lib/featured-images2/featured-images2.php");
	
	//Contact Page Template code
	include(TEMPLATEPATH."/lib/contact-page-plugin/contact-page-plugin.php");
}

add_filter( 'it_featured_images_options', 'it_filter_featured_images_options' );
function it_filter_featured_images_options( $options ) {
	$options['width'] = 380;
	$options['height'] = 250;
	$options['variable_height'] = false;
	
	return $options;
}


///Tracking/Analytics Code
function print_tracking() {
	global $wp_theme_options;
	echo stripslashes($wp_theme_options['tracking']);
}

add_action( 'ithemes_init', 'load_tracking' );
function load_tracking() {
	global $wp_theme_options;
	
	if ( ! empty( $wp_theme_options['tracking'] ) ) {
		if ( 'header' === $wp_theme_options['tracking_pos'] )
			add_action( 'wp_head', 'print_tracking' );
		else
			add_action( 'wp_footer', 'print_tracking' );
	}
}


?>
