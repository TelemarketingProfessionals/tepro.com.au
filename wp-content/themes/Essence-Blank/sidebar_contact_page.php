<!-- begin r_sidebar -->

<div id="r_sidebar">

	<ul id="r_sidebarwidgeted">
	
	<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Contact Page Sidebar') ) : else : ?>


		<li>
		<h2>Get in Touch</h2>
			<ul>
			<li>Phone: 555-5555</li>
			<li>Email: info@yourdomain.com</li>
            <li>Fax: info@yourdomain.com</li>
            <li>Address: 123 All American Bldv.</li>

			</ul>
		</li>
			
	<?php endif; ?>
	
	</ul>
	
</div>

<!-- end r_sidebar -->